<?php
   //////////   ****  Quickkbook Token Update  ***** /////////////////
    function Quickbook_updateOAuth2Token() {


        $company_list = get_data_from_table_list('qb_master','','','','');
        if (is_array($company_list) && count($company_list) > 0 ) {
            foreach($company_list as $_company){
                $ci =& get_instance();
                $ci->load->library('quickbook');
                $updateOAuth2Token = $ci->quickbook->updateOAuth2Token($_company['company_id']);
            }
        }

      
    }

    //////////   ****  Quickkbook All Account  Start  ***** /////////////////
    function Quickbook_AllAccount($company_id = '') {
       
        $ci =& get_instance();
        $table_array = array('Account');
        if($company_id != ''){
            $where_array = array('company_id'=>$company_id);
            $company_list = get_data_from_table_list('qb_master',$where_array,'','','');
        }else{
            $company_list = get_data_from_table_list('qb_master','','','','');
        }
        if (is_array($company_list) && count($company_list) > 0 ) {
            foreach($company_list as $_company){
                    if (is_array($table_array) && count($table_array) > 0 ) {
                        foreach($table_array as $table){
                            $ci->load->library('quickbook');
                            $all_account = $ci->quickbook->AccountFindAll($_company,$table);
                               if ($table == 'Account' && is_array($all_account) && count($all_account) > 0) {
                                   foreach($all_account as $_account){
                                        $check = get_data_from_table('qb_account',array('name'=>$_account->Name,'company_id'=> $_company['company_id']),'','','');
                                        $quickbooks_setting = array();
                                        $quickbooks_setting['company_id'] = $_company['company_id'];
                                        $quickbooks_setting['name'] = isset($_account->Name) ? $_account->Name : '';
                                        $quickbooks_setting['value'] = isset($_account->Id) ? $_account->Id : '';
                                        $quickbooks_setting['account_type'] = isset($_account->AccountType) ? $_account->AccountType : '';
                                        $quickbooks_setting['account_sub_type'] = isset($_account->AccountSubType) ? $_account->AccountSubType : '';
                                        $quickbooks_setting['created_date'] = date('Y-m-d H:i:s');
                                        if (empty($check)) {
                                            grid_add_data($quickbooks_setting,'qb_account');
                                        }else{
                                            grid_data_updates($quickbooks_setting,'qb_account','id',$check['id']);
                                        }
                                   }
                               }
                            }
                        }
                    }
            }
    }

    //////////   ****  Quickkbook All Default Setting Start  ***** /////////////////
    function Quickbook_Default_Setting($company_id = '') {

        $ci =& get_instance();
        $table_array = array('PaymentMethod','Term','Item','TaxCode');
        if($company_id != ''){
            $where_array = array('company_id'=>$company_id);
            $company_list = get_data_from_table_list('qb_master',$where_array,'','','');
        }else{
            $company_list = get_data_from_table_list('qb_master','','','','');
        }
        if (is_array($company_list) && count($company_list) > 0 ) {
            foreach($company_list as $_company){
                    if (is_array($table_array) && count($table_array) > 0 ) {
                        foreach($table_array as $table){
                            $ci->load->library('quickbook');
                            $all_account = $ci->quickbook->AccountFindAll($_company,$table);
                               if ($table == 'PaymentMethod' && is_array($all_account) && count($all_account) > 0) {
                                   foreach($all_account as $_account){
                                        $check = get_data_from_table('qb_payment_method',array('name'=>$_account->Name,'company_id'=>$_company['company_id']),'','','');
                                        $quickbooks_setting = array();
                                        $quickbooks_setting['company_id'] = $_company['company_id'];
                                        $quickbooks_setting['name'] = isset($_account->Name) ? $_account->Name : '';
                                        $quickbooks_setting['value'] = isset($_account->Id) ? $_account->Id : '';
                                        $quickbooks_setting['default'] = '1';
                                        $quickbooks_setting['created_by'] = 0;
                                        $quickbooks_setting['created_date'] = date('Y-m-d H:i:s');
                                        if (empty($check)) {
                                            grid_add_data($quickbooks_setting,'qb_payment_method');
                                        }else{
                                            grid_data_updates($quickbooks_setting,'qb_payment_method','id',$check['id']);
                                        }
                                   }
                               }

                               if ($table == 'Term' && is_array($all_account) && count($all_account) > 0) {
                                   foreach($all_account as $_account){
                                        $check = get_data_from_table('qb_term',array('name'=>$_account->Name,'company_id'=>$_company['company_id']),'','','');
                                        $quickbooks_setting = array();
                                        $quickbooks_setting['company_id'] = $_company['company_id'];
                                        $quickbooks_setting['name'] = isset($_account->Name) ? $_account->Name : '';
                                        $quickbooks_setting['value'] = isset($_account->Id) ? $_account->Id : '';
                                        $quickbooks_setting['default'] = '1';
                                        $quickbooks_setting['created_by'] = 0;
                                        $quickbooks_setting['created_date'] = date('Y-m-d H:i:s');
                                        if (empty($check)) {
                                            grid_add_data($quickbooks_setting,'qb_term');
                                        }else{
                                            grid_data_updates($quickbooks_setting,'qb_term','id',$check['id']);
                                        }
                                   }
                               }

                               if ($table == 'TaxCode' && is_array($all_account) && count($all_account) > 0) {
                                   foreach($all_account as $_account){
                                        $check = get_data_from_table('qb_taxcode',array('name'=>$_account->Name,'company_id'=>$_company['company_id']),'','','');
                                        $quickbooks_setting = array();
                                        $quickbooks_setting['company_id'] = $_company['company_id'];
                                        $quickbooks_setting['name'] = isset($_account->Name) ? $_account->Name : '';
                                        $quickbooks_setting['value'] = isset($_account->Id) ? $_account->Id : '';
                                        $quickbooks_setting['default'] = '1';
                                        $quickbooks_setting['created_by'] = 0;
                                        $quickbooks_setting['created_date'] = date('Y-m-d H:i:s');
                                        if (empty($check)) {
                                            grid_add_data($quickbooks_setting,'qb_taxcode');
                                        }else{
                                            grid_data_updates($quickbooks_setting,'qb_taxcode','id',$check['id']);
                                        }
                                   }
                               }

                            }
                        }
                    }
            }
    }

    function Quickbook_Default_Setting_Add($company_id = '') {
        $ci =& get_instance();
        $table_array = array('PaymentMethod','Term','Item','TaxCode');
        if($company_id != ''){
            $where_array = array('company_id'=>$company_id);
            $company_list = get_data_from_table_list('qb_master',$where_array,'','','');
        }else{
            $company_list = get_data_from_table_list('qb_master','','','','');
        }
        if (is_array($company_list) && count($company_list) > 0 ) {
            foreach($company_list as $_company){
                if (is_array($table_array) && count($table_array) > 0 ) {
                    foreach($table_array as $table){
                        if ($table == 'PaymentMethod') {
                            $payment_method_list = get_data_from_table_list('qb_payment_method',array('value'=>0),'','','');
                            if (is_array($payment_method_list) && count($payment_method_list) > 0 ) {
                                foreach($payment_method_list as $_pname){
                                    $payment_method = array();
                                    $payment_method['name'] = isset($_pname['name']) ? $_pname['name'] : '';
                                    $payment_method = $ci->quickbook->payment_method_Add($_company,$payment_method);
                                    if (isset($payment_method['data']->Id) && $payment_method['data']->Id != '') {
                                        $data = array();
                                        $data['value'] = $payment_method['data']->Id;
                                        grid_data_updates($data,'qb_payment_method','id',$_pname['id']);
                                    }
                                }
                            }
                        }

                        if ($table == 'Term') {
                            $payment_term_list = get_data_from_table_list('qb_term',array('value'=>0),'','','');
                            if (is_array($payment_term_list) && count($payment_term_list) > 0 ) {
                                foreach($payment_term_list as $_pname){
                                    $payment_term = array();
                                    $payment_term['name'] = isset($_pname['name']) ? $_pname['name'] : '';
                                    $payment_term['duedays'] = isset($_pname['duedays']) ? $_pname['duedays'] : '';
                                    $payment_term = $ci->quickbook->Term_Add($_company,$payment_term);
                                    if (isset($payment_term['data']->Id) && $payment_term['data']->Id != '') {
                                        $data = array();
                                        $data['value'] = $payment_term['data']->Id;
                                        grid_data_updates($data,'qb_term','id',$_pname['id']);
                                    }
                                }
                            }
                        }


                        if ($table == 'TaxCode') {
                            $taxcode_list = get_data_from_table_list('qb_taxcode',array('value'=> '' ),'','','');
                            if (is_array($taxcode_list) && count($taxcode_list) > 0 ) {
                                foreach($taxcode_list as $_tname){
                                    $taxcode_arr = array();
                                    $taxcode_arr['name'] = isset($_tname['name']) ? $_tname['name'] : '';
                                    $taxcode_arr['tax_rate'] = isset($_tname['tax_rate']) ? $_tname['tax_rate'] : '';
                                    $taxcode_retuen = $ci->quickbook->TaxService_Add($_company,$taxcode_arr);
                                    if (isset($taxcode_retuen['data']->TaxService->TaxCodeId) && $taxcode_retuen['data']->TaxService->TaxCodeId != '') {
                                        $data = array();
                                        $data['value'] = $taxcode_retuen['data']->TaxService->TaxCodeId;
                                        grid_data_updates($data,'qb_taxcode','id',$_tname['id']);
                                    }
                                }
                            }
                        }



                        if ($table == 'Item') {
                            $item_list = get_data_from_table_list('qb_default_item',array('value'=> '' ),'','','');
                            if (is_array($item_list) && count($item_list) > 0 ) {
                                foreach($item_list as $_iname){
                                    $Item_array =  array (
                                        'TrackQtyOnHand' => true,
                                        'Name' => isset($_iname['name']) ? $_iname['name'] : '',
                                        'QtyOnHand' => 50000000000,
                                    );
                                    $item_retuen = $ci->quickbook->Item_Add($_company,$Item_array);
                                    if (isset($item_retuen['data']->Id) && $item_retuen['data']->Id != '') {
                                        $data = array();
                                        $data['value'] = $item_retuen['data']->Id;
                                        grid_data_updates($data,'qb_default_item','id',$_iname['id']);
                                    }
                                }
                            }
                        }


                    }
                }
            }
        }
    }

    function Quickbook_Customer_Add($Customer_array,$table,$type,$table_id,$log_id,$status,$qb_id) {

        if(isset($customer_array['company_id']) && $customer_array['company_id'] != ''){
            $where_array = array('company_id'=> $customer_array['company_id']);
            $company_data = get_data_from_table('qb_master',$where_array,'','','');
        }else{
            return false;
        }

        $ci =& get_instance();
        $ci->load->library('quickbook');
        $consumer_result = $ci->quickbook->Customer_Add($company_data,$Customer_array);
        $consumer_result_status = array(); 
        if ($consumer_result['status'] == '0') {
            $quickbooks_data['type'] = $type;
            $quickbooks_data['table_id'] = $table_id;
            $quickbooks_data['qb_response'] = json_encode($Customer_array);
            $quickbooks_data['error_message'] = $consumer_result['message'];
            $quickbooks_data['cron_id'] = $log_id;
            $quickbooks_data['qb_add'] = '0';
            if($status == 1){
                grid_add_data($quickbooks_data,$table);
            }elseif($status == 2 && $qb_id != ''){
                grid_data_updates($quickbooks_data,$table,'id',$qb_id);
            }
            $consumer_result_status['message'] = $consumer_result['message'];
            $consumer_result_status['status'] = $consumer_result['status']; 
        }else{
            $consumer_result_status = $consumer_result['status'];
            $quickbooks_table = $table;
            $quickbooks_data = array();
            $quickbooks_data['type'] = $type;
            $quickbooks_data['table_id'] = $table_id;
            $quickbooks_data['qb_id'] = $consumer_result['data']->Id;
            $quickbooks_data['qb_add'] = '1';
            $quickbooks_data['qb_response'] = json_encode($consumer_result['data']);
            $quickbooks_data['error_message'] = $consumer_result['message'];
            $quickbooks_data['cron_id'] = $log_id;
            $logdata['created_date'] = date('Y-m-d H:i:s');
            if($status == 1){
                grid_add_data($quickbooks_data,$table);
            }elseif($status == 2 && $qb_id != ''){
                grid_data_updates($quickbooks_data,$table,'id',$qb_id);
            }
        }
        return $consumer_result_status;
    }

    function Quickbook_Customer_Edit($Customer_array,$table,$wher_column_name,$id,$log_id) {

        if(isset($Customer_array['company_id']) && $Customer_array['company_id'] != ''){
            $where_array = array('company_id'=> $Customer_array['company_id']);
            $company_data = get_data_from_table('qb_master',$where_array,'','','');
        }else{
            return false;
        }

        $ci =& get_instance();
        $ci->load->library('quickbook');
        $consumer_result = $ci->quickbook->Customer_Edit($company_data,$Customer_array);
        $consumer_result_status = array(); 
        if ($consumer_result['status'] == '0') {
            $quickbooks_data['qb_response'] = json_encode($Customer_array);
            $quickbooks_data['error_message'] = $consumer_result['message'];
            $quickbooks_data['cron_id'] = $log_id;
            $quickbooks_data['qb_edit'] = '0';           
            grid_data_updates($quickbooks_data,$table,$wher_column_name,$id);
            $consumer_result_status['message'] = $consumer_result['message'];
            $consumer_result_status['status'] = $consumer_result['status']; 
        }else{
            $consumer_result_status = $consumer_result['status'];
            $quickbooks_table_id = $id;
            $quickbooks_table = $table;
            $quickbooks_data = array();
            $quickbooks_table_colum = $wher_column_name;
            $quickbooks_data['qb_edit'] = '1';
            $quickbooks_data['qb_response'] = json_encode($consumer_result['data']);
            $quickbooks_data['error_message'] = $consumer_result['message'];
            $quickbooks_data['cron_id'] = $log_id;
            grid_data_updates($quickbooks_data,$quickbooks_table,$quickbooks_table_colum,$quickbooks_table_id);
        }
        return $consumer_result_status;
    }

    //////////   ****  Quickkbook Invoice Add ***** /////////////////
    function Quickbook_Invoice_Add($Item_array,$table,$wher_column_name,$id) {

        if(isset($Item_array['company_id']) && $Item_array['company_id'] != ''){
            $where_array = array('company_id'=> $Item_array['company_id']);
            $company_data = get_data_from_table('qb_master',$where_array,'','','');
        }else{
            return false;
        }

        $ci =& get_instance();
        $ci->load->library('quickbook');
        $invoice_result = $ci->quickbook->Invoice_Add($company_data,$Item_array);
        $invoice_result_status = array(); 
        if ($invoice_result['status'] == '0') {
            $quickbooks_data['quickbooks_data'] = json_encode($Item_array);
            $quickbooks_data['error_massage'] = $invoice_result['message'];
            $quickbooks_data['action_error'] = 'Add';
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$table,$wher_column_name,$id);
            $invoice_result_status['message'] = $invoice_result['message'];
            $invoice_result_status['status'] = $invoice_result['status']; 
        }else{
            $invoice_result_status = $invoice_result['status'];
            $quickbooks_table_id = $id;
            $quickbooks_table = $table;
            $quickbooks_data = array();
            $quickbooks_data['quickbooks_responce'] = json_encode($invoice_result['data']);
            $quickbooks_data['push_in_qb'] = 1;
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$quickbooks_table,$quickbooks_table_colum,$quickbooks_table_id);
        }
        return $invoice_result_status;
    }

    //////////   ****  Quickkbook Invoice Edit ***** /////////////////
    function Quickbook_Invoice_Edit($Invoice_array,$table,$wher_column_name,$id) {
            
            if(isset($Invoice_array['company_id']) && $Invoice_array['company_id'] != ''){
                $where_array = array('company_id'=> $Invoice_array['company_id']);
                $company_data = get_data_from_table('qb_master',$where_array,'','','');
            }else{
                return false;
            }

            $ci =& get_instance();
            $ci->load->library('quickbook');

            $invoice_result = $ci->quickbook->Invoice_Edit($company_data,$Invoice_array);
                $invoice_result_status = array(); 
            if ($invoice_result['status'] == '0') {
                $quickbooks_data['quickbooks_data'] = json_encode($Invoice_array);
                $quickbooks_data['error_massage'] = $invoice_result['message'];
                $quickbooks_data['action_error'] = 'Edit';
                $quickbooks_table_colum = $wher_column_name;
                grid_data_updates($quickbooks_data,$table,$wher_column_name,$id);
                $invoice_result_status['message'] = $invoice_result['message'];
                $invoice_result_status['status'] = $invoice_result['status']; 
            }else{
                $invoice_result_status = $invoice_result['status'];
                $quickbooks_table_id = $id;
                $quickbooks_table = $table;
                $quickbooks_data = array();
                $quickbooks_data['quickbooks_responce'] = json_encode($invoice_result['data']);
                $quickbooks_data['push_in_qb'] = 1;
                $quickbooks_table_colum = $wher_column_name;
                grid_data_updates($quickbooks_data,$quickbooks_table,$quickbooks_table_colum,$quickbooks_table_id);
            }
            return $invoice_result_status;
    }

     //////////   ****  Quickkbook Rounding Invoice ***** /////////////////
    function Quickbook_Rounding_invoice($Invoice_array,$table,$wher_column_name,$id) {
            
        if(isset($Invoice_array['company_id']) && $Invoice_array['company_id'] != ''){
            $where_array = array('company_id'=> $Invoice_array['company_id']);
            $company_data = get_data_from_table('qb_master',$where_array,'','','');
        }else{
            return false;
        }

        $ci =& get_instance();
        $ci->load->library('quickbook');

        $invoice_result = $ci->quickbook->Invoice_Rounding($company_data,$Invoice_array);
            $invoice_result_status = array(); 
        if ($invoice_result['status'] == '0') {
            $quickbooks_data['quickbooks_data'] = json_encode($Invoice_array);
            $quickbooks_data['error_massage'] = $invoice_result['message'];
            $quickbooks_data['action_error'] = 'Edit';
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$table,$wher_column_name,$id);
            $invoice_result_status['message'] = $invoice_result['message'];
            $invoice_result_status['status'] = $invoice_result['status']; 
        }else{
            $invoice_result_status = $invoice_result['status'];
            $quickbooks_table_id = $id;
            $quickbooks_table = $table;
            $quickbooks_data = array();
            $quickbooks_data['quickbooks_responce'] = json_encode($invoice_result['data']);
            $quickbooks_data['push_in_qb'] = 1;
            $quickbooks_data['push_rounding_inv_in_qb'] = 1;
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$quickbooks_table,$quickbooks_table_colum,$quickbooks_table_id);
        }
        return $invoice_result_status;
    }

    //////////   ****  Quickkbook Item Add ***** /////////////////
    function Quickbook_Item_Add($Item_array,$table,$wher_column_name,$id) {
            
        if(isset($Item_array['company_id']) && $Item_array['company_id'] != ''){
            $where_array = array('company_id'=> $Item_array['company_id']);
            $company_data = get_data_from_table('qb_master',$where_array,'','','');
        }else{
            return false;
        }


        $ci =& get_instance();
        $ci->load->library('quickbook');
        $item_result = $ci->quickbook->Item_Add($company_data,$Item_array);
        $item_result_status = array(); 
        if ($item_result['status'] == '0') {
            $quickbooks_data = array();
            $quickbooks_data['quickbooks_data'] = json_encode($Item_array);
            $quickbooks_data['error_massage'] = $item_result['message'];
            $quickbooks_data['action_error'] = 'Add';
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$table,$wher_column_name,$id);
            $item_result_status['message'] = $item_result['message'];
            $item_result_status['status'] = $item_result['status']; 
        }else{
            $item_result_status = $item_result['status'];
            $quickbooks_table_id = $id;
            $quickbooks_table = $table;
            $quickbooks_data = array();
            $quickbooks_data['quickbooks_responce'] = json_encode($item_result['data']);
            $quickbooks_data['push_in_qb'] = 1;
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$quickbooks_table,$quickbooks_table_colum,$quickbooks_table_id);
            $data =  array();
            $data['item_id'] = $id;
            $data['type'] = $table;
            $table_qb = 'qb_contact';
            grid_add_data($data,$table_qb);
        }
        return $item_result_status;
    }

    //////////   ****  Quickkbook Item Edit ***** /////////////////
    function Quickbook_Item_Edit($Item_array,$table,$wher_column_name,$id) {
            
        if(isset($Item_array['company_id']) && $Item_array['company_id'] != ''){
            $where_array = array('company_id'=> $Item_array['company_id']);
            $company_data = get_data_from_table('qb_master',$where_array,'','','');
        }else{
            return false;
        }

        $ci =& get_instance();
        $ci->load->library('quickbook');
        $item_result = $ci->quickbook->Item_Edit($company_data,$Item_array);
        $item_result_status = array(); 
        if ($item_result['status'] == '0') {
            $quickbooks_data = array();
            $quickbooks_data['quickbooks_data'] = json_encode($Item_array);
            $quickbooks_data['error_massage'] = $item_result['message'];
            $quickbooks_data['action_error'] = 'Edit';
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$table,$wher_column_name,$id);
            $item_result_status['message'] = $item_result['message'];
            $item_result_status['status'] = $item_result['status']; 
        }else{
            $item_result_status = $item_result['status'];
            $quickbooks_table_id = $id;
            $quickbooks_table = $table;
            $quickbooks_data = array();
            $quickbooks_data['quickbooks_responce'] = json_encode($item_result['data']);
            $quickbooks_data['push_in_qb'] = 1;
            $quickbooks_table_colum = $wher_column_name;
        }
        return $item_result_status;
    }

    //////////   ****  Quickkbook Payment Add ***** /////////////////
    function Quickbook_Payment_Add($Payment_array,$table,$wher_column_name,$id) {
            
        if(isset($Payment_array['company_id']) && $Payment_array['company_id'] != ''){
            $where_array = array('company_id'=> $Payment_array['company_id']);
            $company_data = get_data_from_table('qb_master',$where_array,'','','');
        }else{
            return false;
        }

        $ci =& get_instance();
        $ci->load->library('quickbook');
        $payment_result = $ci->quickbook->Payment_Add($company_data,$Payment_array);
        $payment_result_status = array(); 
        if ($payment_result['status'] == '0') {
            $quickbooks_data['quickbooks_data'] = json_encode($Payment_array);
            $quickbooks_data['error_message'] = $payment_result['message'];
            $quickbooks_data['action_error'] = 'Add';
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$table,$wher_column_name,$id);
            $payment_result_status['message'] = $payment_result['message'];
            $payment_result_status['status'] = $payment_result['status']; 
        }else{
            $payment_result_status['status'] = $payment_result['status'];
            $payment_result_status['data'] = $payment_result['data'];
            $quickbooks_table_id = $id;
            $quickbooks_table = $table;
            $quickbooks_data = array();
            $quickbooks_data['quickbooks_responce'] = json_encode($payment_result['data']);
            $quickbooks_data['push_in_qb'] = 1;
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$quickbooks_table,$quickbooks_table_colum,$quickbooks_table_id);
        }
        return $payment_result_status;
    }

    //////////   ****  Quickkbook Payment Edit ***** /////////////////
    function Quickbook_Payment_Edit($Payment_array,$table,$wher_column_name,$id) {
            
        if(isset($Item_array['company_id']) && $Item_array['company_id'] != ''){
            $where_array = array('company_id'=> $Item_array['company_id']);
            $company_data = get_data_from_table('qb_master',$where_array,'','','');
        }else{
            return false;
        }

        $ci =& get_instance();
        $ci->load->library('quickbook');
        
        $payment_result = $ci->quickbook->Payment_Edit($company_data,$Payment_array);
        $payment_result_status = array(); 
        if ($payment_result['status'] == '0') {
            $quickbooks_data['quickbooks_data'] = json_encode($Payment_array);
            $quickbooks_data['error_message'] = $payment_result['message'];
            $quickbooks_data['action_error'] = 'Edit';
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$table,$wher_column_name,$id);
            $payment_result_status['message'] = $payment_result['message'];
            $payment_result_status['status'] = $payment_result['status']; 
        }else{
            $payment_result_status = $payment_result['status'];
            $quickbooks_table_id = $id;
            $quickbooks_table = $table;
            $quickbooks_data = array();
            $quickbooks_data['quickbooks_responce'] = json_encode($payment_result['data']);
            $quickbooks_data['push_in_qb'] = 1;
            $quickbooks_table_colum = $wher_column_name;
             
            grid_data_updates($quickbooks_data,$quickbooks_table,$quickbooks_table_colum,$quickbooks_table_id);
        }
        return $payment_result_status;
    }

    //////////   ****  Quickkbook Payment Edit With Credit Note ***** /////////////////
    function Quickbook_payment_edit_with_creditnote($Payment_array,$table,$wher_column_name,$id) {
        if(isset($Payment_array['company_id']) && $Payment_array['company_id'] != ''){
            $where_array = array('company_id'=> $Payment_array['company_id']);
            $company_data = get_data_from_table('qb_master',$where_array,'','','');
        }else{
            return false;
        }

        $ci =& get_instance();
        $ci->load->library('quickbook');
        $payment_result = $ci->quickbook->Payment_Edit_with_creditnote($company_data,$Payment_array);
        $payment_result_status = array(); 
        if ($payment_result['status'] == '0') {
            $quickbooks_data['quickbooks_data'] = json_encode($Payment_array);
            $quickbooks_data['error_message'] = $payment_result['message'];
            $quickbooks_data['action_error'] = 'Edit';
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$table,$wher_column_name,$id);
            $payment_result_status['message'] = $payment_result['message'];
            $payment_result_status['status'] = $payment_result['status']; 
        }else{
            $payment_result_status = $payment_result['status'];
            $quickbooks_table_id = $id;
            $quickbooks_table = $table;
            $quickbooks_data = array();
            $quickbooks_data['quickbooks_responce'] = json_encode($payment_result['data']);
            $quickbooks_data['push_in_qb'] = 1;
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$quickbooks_table,$quickbooks_table_colum,$quickbooks_table_id);
        }
        return $payment_result_status;
    }
      //////////   ****  Quickkbook Credit Note ***** /////////////////
    function Quickbook_CreditNote_Add($Credit_note_array,$table,$wher_column_name,$id) {
            if(isset($Credit_note_array['company_id']) && $Credit_note_array['company_id'] != ''){
                $where_array = array('company_id'=> $Credit_note_array['company_id']);
                $company_data = get_data_from_table('qb_master',$where_array,'','','');
            }else{
                return false;
            }

            $ci =& get_instance();
            $ci->load->library('quickbook');

            $creditnote_result = $ci->quickbook->CreditNote_Add($company_data,$Credit_note_array);
            $creditnote_result_status = array(); 
            if ($creditnote_result['status'] == '0') {
                $quickbooks_data['quickbooks_data'] = json_encode($Credit_note_array);
                $quickbooks_data['error_massage'] = $creditnote_result['message'];
                $quickbooks_data['action_error'] = 'Add';
                $quickbooks_table_colum = $wher_column_name;
                grid_data_updates($quickbooks_data,$table,$wher_column_name,$id);
                $creditnote_result_status['message'] = $creditnote_result['message'];
                $creditnote_result_status['status'] = $creditnote_result['status']; 
            }else{
                $creditnote_result_status = $creditnote_result['status'];
                $quickbooks_table_id = $id;
                $quickbooks_table = $table;
                $quickbooks_data = array();
                $quickbooks_data['quickbooks_responce'] = json_encode($creditnote_result['data']);
                $quickbooks_data['push_in_qb'] = 1;
                $quickbooks_table_colum = $wher_column_name;
                grid_data_updates($quickbooks_data,$quickbooks_table,$quickbooks_table_colum,$quickbooks_table_id);
            }
            return $creditnote_result_status;
    }

    //////////   ****  Quickkbook Void Invoice ***** /////////////////
    function Quickbook_VoidInvoice($Invoice_array,$table,$wher_column_name,$id) {
        if(isset($Invoice_array['company_id']) && $Invoice_array['company_id'] != ''){
            $where_array = array('company_id'=> $Invoice_array['company_id']);
            $company_data = get_data_from_table('qb_master',$where_array,'','','');
        }else{
            return false;
        }

        $ci =& get_instance();
        $ci->load->library('quickbook');
        $invoice_result = $ci->quickbook->Void_invoice($company_data,$Invoice_array);
    
        $invoice_result_status = array(); 
        if ($invoice_result['status'] == '0') {
            $quickbooks_data['quickbooks_data'] = json_encode($Invoice_array);
            $quickbooks_data['error_massage'] = $invoice_result['message'];
            $quickbooks_data['action_error'] = 'Void';
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$table,$wher_column_name,$id);
            $invoice_result_status['message'] = $invoice_result['message'];
            $invoice_result_status['status'] = $invoice_result['status']; 
        }else{
            $invoice_result_status = $invoice_result['status'];
            $quickbooks_table_id = $id;
            $quickbooks_table = $table;
            $quickbooks_data = array();
            $quickbooks_data['quickbooks_responce'] = json_encode($invoice_result['data']);
            $quickbooks_data['push_in_qb'] = 1;
            $quickbooks_data['push_in_cancel_qb'] = 1;
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$quickbooks_table,$quickbooks_table_colum,$quickbooks_table_id);
        }
        return $invoice_result_status;
    }

     //////////   ****  Quickkbook Void CreditNOte ***** /////////////////
    function Quickbook_VoidCreditNote($Invoice_array,$table,$wher_column_name,$id) {
        if(isset($Invoice_array['company_id']) && $Invoice_array['company_id'] != ''){
            $Invoice_array = array('company_id'=> $Invoice_array['company_id']);
            $company_data = get_data_from_table('qb_master',$Invoice_array,'','','');
        }else{
            return false;
        }

        $ci =& get_instance();
        $ci->load->library('quickbook');
        $invoice_result = $ci->quickbook->Void_creditnote($company_data,$Invoice_array);

        $invoice_result_status = array(); 
        if ($invoice_result['status'] == '0') {
            $quickbooks_data['quickbooks_data'] = json_encode($Invoice_array);
            $quickbooks_data['error_massage'] = $invoice_result['message'];
            $quickbooks_data['action_error'] = 'Void';
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$table,$wher_column_name,$id);
            $invoice_result_status['message'] = $invoice_result['message'];
            $invoice_result_status['status'] = $invoice_result['status']; 
        }else{
            $invoice_result_status = $invoice_result['status'];
            $quickbooks_table_id = $id;
            $quickbooks_table = $table;
            $quickbooks_data = array();
            $quickbooks_data['quickbooks_responce'] = json_encode($invoice_result['data']);
            $quickbooks_data['push_in_qb'] = 1;
            $quickbooks_data['push_in_cancel_qb'] = 1;
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$quickbooks_table,$quickbooks_table_colum,$quickbooks_table_id);
        }
        return $invoice_result_status;
    }

    //////////   ****  Quickkbook Void Payment ***** /////////////////
    function Quickbook_Void_Payment($Payment_array,$table,$wher_column_name,$id) {
        if(isset($Payment_array['company_id']) && $Payment_array['company_id'] != ''){
            $where_array = array('company_id'=> $Payment_array['company_id']);
            $company_data = get_data_from_table('qb_master',$where_array,'','','');
        }else{
            return false;
        }
        $ci =& get_instance();
        $ci->load->library('quickbook');
        $payment_result = $ci->quickbook->Void_payment($company_data,$Payment_array);
        $payment_result_status = array(); 
        if ($payment_result['status'] == '0') {
             $quickbooks_data['quickbooks_data'] = json_encode($Payment_array);
            $quickbooks_data['error_message'] = $payment_result['message'];
            $quickbooks_data['action_error'] = 'Void';
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$table,$wher_column_name,$id);
            $payment_result_status['message'] = $payment_result['message'];
            $payment_result_status['status'] = $payment_result['status']; 
        }else{
            $payment_result_status = $payment_result['status'];
            $quickbooks_table_id = $id;
            $quickbooks_table = $table;
            $quickbooks_data = array();
            $quickbooks_data['quickbooks_responce'] = json_encode($payment_result['data']);
            $quickbooks_data['push_in_qb'] = 1;
            $quickbooks_data['push_in_cancel_qb'] = 1;
            $quickbooks_table_colum = 'id';
            grid_data_updates($quickbooks_data,$quickbooks_table,$quickbooks_table_colum,$quickbooks_table_id);
        }
        return $payment_result_status;
    }

    //////////   ****  Quickkbook Refund Invoice  ***** /////////////////
    function Quickbook_RefundInvoice_Add($Refund_array,$table,$wher_column_name,$id) {
        if(isset($Refund_array['company_id']) && $Refund_array['company_id'] != ''){
            $where_array = array('company_id'=> $Refund_array['company_id']);
            $company_data = get_data_from_table('qb_master',$where_array,'','','');
        }else{
            return false;
        }
        $ci =& get_instance();
        $ci->load->library('quickbook');
        $refund_result = $ci->quickbook->Refund_invoice_Add($company_data,$Refund_array);
        $refund_result_status = array(); 
        if ($refund_result['status'] == '0') {
             $quickbooks_data['quickbooks_data'] = json_encode($Refund_array);
            $quickbooks_data['error_massage'] = $refund_result['message'];
            $quickbooks_data['action_error'] = 'Refund Add';
            $quickbooks_table_colum = $wher_column_name;
            grid_data_updates($quickbooks_data,$table,$wher_column_name,$id);
            $refund_result_status['message'] = $refund_result['message'];
            $refund_result_status['status'] = $refund_result['status']; 
        }else{
            $refund_result_status = $refund_result['status'];
            $quickbooks_table_id = $id;
            $quickbooks_table = $table;
            $quickbooks_data = array();
            $quickbooks_data['quickbooks_responce'] = json_encode($refund_result['data']);
            $quickbooks_data['push_in_qb'] = 1;
            $quickbooks_table_colum = 'id';
            grid_data_updates($quickbooks_data,$quickbooks_table,$quickbooks_table_colum,$quickbooks_table_id);
        }
        return $refund_result_status;
    }

?>