


DROP TABLE IF EXISTS `qb_account`;
CREATE TABLE IF NOT EXISTS `qb_account` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` int NOT NULL,
  `account_type` varchar(255) NOT NULL,
  `account_sub_type` varchar(255) NOT NULL,
  `created_by` int NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int NOT NULL,
  `updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `qb_account_code_setting`
--

DROP TABLE IF EXISTS `qb_account_code_setting`;
CREATE TABLE IF NOT EXISTS `qb_account_code_setting` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_id` int NOT NULL,
  `invoice_course_debit_account_id` int NOT NULL,
  `invoice_course_credit_account_id` int NOT NULL,
  `invoice_course_future_debit_account_id` int NOT NULL,
  `invoice_course_future_credit_account_id` int NOT NULL,
  `misc_invoice_course_debit_account_id` int NOT NULL,
  `misc_invoice_course_credit_account_id` int NOT NULL,
  `invoice_discount_account_id` int NOT NULL,
  `invoice_grant_account_id` int NOT NULL,
  `invoice_gst_account_id` int NOT NULL,
  `invoice_other_debit_account_id` int NOT NULL,
  `invoice_other_credit_account_id` int NOT NULL,
  `credit_note_invoice_debit_account_id` int NOT NULL,
  `credit_note_invoice_credit_account_id` int NOT NULL,
  `credit_note_refund_invoice_debit_account_id` int NOT NULL,
  `credit_refund_note_invoice_credit_account_id` int NOT NULL,
  `receipt_debit_account_id` int NOT NULL,
  `receipt_credit_account_id` int NOT NULL,
  `refund_invoice_debit_account_id` int NOT NULL,
  `refund_invoice_credit_account_id` int NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_account_code_setting_log`
--

DROP TABLE IF EXISTS `qb_account_code_setting_log`;
CREATE TABLE IF NOT EXISTS `qb_account_code_setting_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_id` int NOT NULL,
  `invoice_course_debit_account_id` int NOT NULL,
  `invoice_course_credit_account_id` int NOT NULL,
  `invoice_course_future_debit_account_id` int NOT NULL,
  `invoice_course_future_credit_account_id` int NOT NULL,
  `misc_invoice_course_debit_account_id` int NOT NULL,
  `misc_invoice_course_credit_account_id` int NOT NULL,
  `invoice_discount_account_id` int NOT NULL,
  `invoice_grant_account_id` int NOT NULL,
  `invoice_gst_account_id` int NOT NULL,
  `invoice_other_debit_account_id` int NOT NULL,
  `invoice_other_credit_account_id` int NOT NULL,
  `credit_note_invoice_debit_account_id` int NOT NULL,
  `credit_note_invoice_credit_account_id` int NOT NULL,
  `credit_note_refund_invoice_debit_account_id` int NOT NULL,
  `credit_refund_note_invoice_credit_account_id` int NOT NULL,
  `receipt_debit_account_id` int NOT NULL,
  `receipt_credit_account_id` int NOT NULL,
  `refund_invoice_debit_account_id` int NOT NULL,
  `refund_invoice_credit_account_id` int NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_default_item`
--

DROP TABLE IF EXISTS `qb_default_item`;
CREATE TABLE IF NOT EXISTS `qb_default_item` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `default` int NOT NULL,
  `created_by` int NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `qb_default_item`
--

INSERT INTO `qb_default_item` (`id`, `company_id`, `name`, `value`, `default`, `created_by`, `created_date`) VALUES
(1, 1, 'Skill Future Grant', '21', 0, 1, '2021-09-27 05:47:36'),
(2, 1, 'Skill Future Credit', '22', 0, 1, '2021-09-27 13:22:11'),
(3, 1, 'Credi Note Item', '22', 0, 1, '2021-09-27 13:22:11'),
(4, 1, 'Refund Invoice Item', '22', 0, 1, '2021-09-27 13:22:11');

-- --------------------------------------------------------

--
-- Table structure for table `qb_master`
--

DROP TABLE IF EXISTS `qb_master`;
CREATE TABLE IF NOT EXISTS `qb_master` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_id` int NOT NULL,
  `status` enum('A','I','','') NOT NULL,
  `auth_mode` varchar(255) NOT NULL,
  `ClientID` varchar(255) NOT NULL,
  `ClientSecret` varchar(255) NOT NULL,
  `accessTokenKey` longtext NOT NULL,
  `refreshTokenKey` varchar(255) NOT NULL,
  `QBORealmID` varchar(255) NOT NULL,
  `baseUrl` enum('Production','Development') NOT NULL,
  `last_updated_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `qb_payment_method`
--

DROP TABLE IF EXISTS `qb_payment_method`;
CREATE TABLE IF NOT EXISTS `qb_payment_method` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` int NOT NULL,
  `default` int NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `qb_taxcode`
--

DROP TABLE IF EXISTS `qb_taxcode`;
CREATE TABLE IF NOT EXISTS `qb_taxcode` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `tax_rate` varchar(255) NOT NULL,
  `default` int NOT NULL,
  `created_by` int NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `qb_term`
--

DROP TABLE IF EXISTS `qb_term`;
CREATE TABLE IF NOT EXISTS `qb_term` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` int NOT NULL,
  `duedays` varchar(255) NOT NULL,
  `default` int NOT NULL,
  `created_by` int NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
