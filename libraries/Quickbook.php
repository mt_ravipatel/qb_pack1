<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');  
require_once(APPPATH."third_party/Quickbook/vendor/autoload.php");
require_once(APPPATH."third_party/Quickbook/src/config.php");

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Core\CoreConstants;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\CreditMemo;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\PaymentMethod;
use QuickBooksOnline\API\Facades\Term;
use QuickBooksOnline\API\Facades\TaxService;
use QuickBooksOnline\API\Facades\TaxRate;
use QuickBooksOnline\API\Facades\Item;
use QuickBooksOnline\API\Facades\RefundReceipt;
use QuickBooksOnline\API\Core\OAuth\OAuth2\OAuth2LoginHelper;

class Quickbook {
	public $dataService;
	public $quickbook_log_dir;

	public function __construct(){
        if (isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == 'localhost') {
          $quickbook_log_dir = $_SERVER['DOCUMENT_ROOT'].'/application/logs/QuickbookLog';
        }else{
          $quickbook_log_dir = $_SERVER['DOCUMENT_ROOT'].'/application/logs/QuickbookLog';
        }

        if (!is_dir($quickbook_log_dir)) {
        	mkdir($quickbook_log_dir);
        }
    }

	public function AccountFindAll($qb_config,$table){
		$i = 1;
		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));
		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);
	    $allAccounts = $this->dataService->FindAll($table, $i, 500);
	    $error = $this->dataService->getLastError();
	    if ($error) {
	    	$error_code = '';
	    	$return_array = array();
	    	$error_code .= "The Status code is  : " . $error->getHttpStatusCode() . "\n";
	    	$error_code .= "The Helper message is : " . $error->getOAuthHelperError() . "\n";
	    	$error_code .= "The Response message is : " . $error->getResponseBody() . "\n";
	    	return array('error'=>$error_code);
	    }
	    $return_array = array();
	    $return_array['file_path'] = $filepath;
	    $return_array['data'] = $allAccounts;
	    return $return_array;
	}

	public function updateOAuth2Token($qb_config){
		$oauth2LoginHelper = new OAuth2LoginHelper(isset($qb_config) ? $qb_config['ClientID'] : '',isset($qb_config) ? $qb_config['ClientSecret'] : '');
		$accessTokenObj = $oauth2LoginHelper->
		                    refreshAccessTokenWithRefreshToken(isset($qb_config) ? $qb_config['refreshTokenKey'] : '');
		$accessTokenValue = $accessTokenObj->getAccessToken();
		$refreshTokenValue = $accessTokenObj->getRefreshToken();
		if ($accessTokenValue != '' && $refreshTokenValue != '') {
			$return_array = array();
			$return_array['accessTokenKey'] = $accessTokenValue;
			$return_array['refreshTokenKey'] = $refreshTokenValue;
			return $return_array;
		}
	}

	public function Customer_Add($qb_config,$Customer_array){
		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));
		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);
		$entities = $this->dataService->Query("SELECT * FROM Customer where GivenName='".$Customer_array['GivenName']."'");
		$error = $this->dataService->getLastError();
		$return = '';
		if ($error) {
			$error->getResponseBody();
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			return $return;
		}
		if(empty($entities)){
			$customerObj = Customer::create([
				  "BillAddr" => [
				     "Line1"=> isset($Customer_array['BillAddr']['Line1']) ? $Customer_array['BillAddr']['Line1'] : '',
				     "Line2"=> isset($Customer_array['BillAddr']['Line2']) ? $Customer_array['BillAddr']['Line2'] : '',
				     "City"=>  isset($Customer_array['BillAddr']['City']) ? $Customer_array['BillAddr']['City'] : '',
				     "Country"=>  isset($Customer_array['BillAddr']['Country']) ? $Customer_array['BillAddr']['Country'] : '',
				     "CountrySubDivisionCode"=> isset($Customer_array['BillAddr']['CountrySubDivisionCode']) ? $Customer_array['BillAddr']['CountrySubDivisionCode'] : '',
				     "PostalCode"=>  (isset($Customer_array['BillAddr']['PostalCode']) && $Customer_array['BillAddr']['PostalCode'] != '0') ? $Customer_array['BillAddr']['PostalCode'] : '',
				 ],
				 'ShipAddr' => [
					     	 'Line1'=>  isset($Customer_array['ShipAddr']['Line1']) ? $Customer_array['ShipAddr']['Line1'] : ((isset($theCustomer->ShipAddr->Line1)) ? $theCustomer->ShipAddr->Line1 : '' ),
					     	 'City'=>  isset($Customer_array['ShipAddr']['City']) ? $Customer_array['ShipAddr']['City'] : ((isset($theCustomer->ShipAddr->City)) ? $theCustomer->ShipAddr->City : '' ),
					     	 'Country'=>  isset($Customer_array['ShipAddr']['Country']) ? $Customer_array['ShipAddr']['Country'] : ((isset($theCustomer->ShipAddr->Country)) ? $theCustomer->ShipAddr->Country : '' ),
					     	 'CountrySubDivisionCode'=>  isset($Customer_array['ShipAddr']['CountrySubDivisionCode']) ? $Customer_array['ShipAddr']['CountrySubDivisionCode'] : ((isset($theCustomer->ShipAddr->CountrySubDivisionCode)) ? $theCustomer->ShipAddr->CountrySubDivisionCode : '' ) ,
					     	 'PostalCode'=>  (isset($Customer_array['ShipAddr']['PostalCode']) && $Customer_array['ShipAddr']['PostalCode'] != '0') ? $Customer_array['ShipAddr']['PostalCode'] : ((isset($theCustomer->ShipAddr->PostalCode)) ? $theCustomer->ShipAddr->PostalCode : '' ),
				 	],
				 "Notes" =>  isset($Customer_array['Notes']) ? $Customer_array['Notes'] : '',
				 "Title"=>   isset($Customer_array['Title']) ? $Customer_array['Title'] : '',
				 "GivenName"=> isset($Customer_array['GivenName']) ? $Customer_array['GivenName'] : '',
				 "MiddleName"=>  isset($Customer_array['MiddleName']) ? $Customer_array['MiddleName'] : '',
				 //"FamilyName"=>  isset($Customer_array['FamilyName']) ? $Customer_array['FamilyName'] : '',
				 "FullyQualifiedName"=>   isset($Customer_array['FullyQualifiedName']) ? $Customer_array['FullyQualifiedName'] : '',
				 "CompanyName"=>  isset($Customer_array['CompanyName']) ? $Customer_array['CompanyName'] : '',
				 "DisplayName"=>  isset($Customer_array['DisplayName']) ? $Customer_array['DisplayName'] : '',
				 "PrimaryPhone"=>  [
				     "FreeFormNumber"=>  isset($Customer_array['PrimaryPhone']['FreeFormNumber']) ? $Customer_array['PrimaryPhone']['FreeFormNumber'] : '',
				 ],
				 "PrimaryEmailAddr"=>  [
				     "Address"=>  isset($Customer_array['PrimaryEmailAddr']['Address']) ? $Customer_array['PrimaryEmailAddr']['Address'] : '',
				 ]
				]);
				$resultingCustomerObj = $this->dataService->Add($customerObj);
				$error = $this->dataService->getLastError();
				$return = '';
				if ($error) {
					$return['status'] = 0 ; 
					$return['message'] = $error->getResponseBody();
					$return['file_path'] = $filepath;
				}else {
					$return['status'] = 1; 
					$return['message'] = 'success'; 
					$return['data'] = $resultingCustomerObj;
					$return['file_path'] = $filepath;
				}
		}else{
				$return['status'] = 1; 
				$return['message'] = 'success'; 
				$return['data'] = reset($entities);
				$return['file_path'] = $filepath;
		}
		return  $return ;
	}

	public function Customer_Edit($qb_config,$Customer_array){
		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));
		$filepath = $this->dataService->setLogLocation($quickbook_log_dir);

		$entities = $this->dataService->Query("SELECT * FROM Customer where Id='".$Customer_array['Id']."'");
		$error = $this->dataService->getLastError();
		$return = '';
		if ($error) {
			$error->getResponseBody();
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
			return $return;
		}
		if(empty($entities)){
			$return['status'] = 0 ; 
			$return['message'] = 'No Data Found';
			$return['file_path'] = $filepath;
			return $return;	
		}
		$theCustomer = reset($entities);
			$updateCustomer = Customer::update($theCustomer, [
			    'sparse' => 'false',
			    'DisplayName' => (isset($Customer_array['DisplayName'])) ? $Customer_array['DisplayName'] : ((isset($theCustomer->DisplayName))  ? $theCustomer->DisplayName : " "),
			    'PrimaryEmailAddr' => [
		     	 'Address'=> isset($Customer_array['PrimaryEmailAddr']['Address']) ? $Customer_array['PrimaryEmailAddr']['Address'] :  ((isset($theCustomer->PrimaryEmailAddr->Address)) ? $theCustomer->PrimaryEmailAddr->Address : " " ) ,
		 		],
		 		'PrimaryPhone' => [
		     	 'FreeFormNumber'=>  isset($Customer_array['PrimaryPhone']['FreeFormNumber']) ? $Customer_array['PrimaryPhone']['FreeFormNumber'] : ((isset($theCustomer->PrimaryPhone->FreeFormNumber)) ? $theCustomer->PrimaryPhone->FreeFormNumber : '' ),
		 		],
		 		'Title' => isset($Customer_array['Title']) ? $Customer_array['Title'] : ((isset($theCustomer->Title)) ? $theCustomer->Title : ''),
		 		'BillAddr' => [
			     	 'Line1'=>  isset($Customer_array['BillAddr']['Line1']) ? $Customer_array['BillAddr']['Line1'] : ((isset($theCustomer->BillAddr->Line1)) ? $theCustomer->BillAddr->Line1 : '' ),
			     	 "Line2"=> isset($Customer_array['BillAddr']['Line2']) ? $Customer_array['BillAddr']['Line2'] : '',
			     	 'City'=>  isset($Customer_array['BillAddr']['City']) ? $Customer_array['BillAddr']['City'] : ((isset($theCustomer->BillAddr->City)) ? $theCustomer->BillAddr->City : '' ),
			     	 'Country'=>  isset($Customer_array['BillAddr']['Country']) ? $Customer_array['BillAddr']['Country'] : ((isset($theCustomer->BillAddr->Country)) ? $theCustomer->BillAddr->Country : '' ),
			     	 'CountrySubDivisionCode'=>  isset($Customer_array['BillAddr']['CountrySubDivisionCode']) ? $Customer_array['BillAddr']['CountrySubDivisionCode'] : ((isset($theCustomer->BillAddr->CountrySubDivisionCode)) ? $theCustomer->BillAddr->CountrySubDivisionCode : '' ) ,
			     	 'PostalCode'=>  (isset($Customer_array['BillAddr']['PostalCode']) && $Customer_array['BillAddr']['PostalCode'] != '0') ? $Customer_array['BillAddr']['PostalCode'] : ((isset($theCustomer->BillAddr->PostalCode)) ? $theCustomer->BillAddr->PostalCode : '' ),
		 		],
		 		'ShipAddr' => [
			     	 'Line1'=>  isset($Customer_array['ShipAddr']['Line1']) ? $Customer_array['ShipAddr']['Line1'] : ((isset($theCustomer->ShipAddr->Line1)) ? $theCustomer->ShipAddr->Line1 : '' ),
			     	 'City'=>  isset($Customer_array['ShipAddr']['City']) ? $Customer_array['ShipAddr']['City'] : ((isset($theCustomer->ShipAddr->City)) ? $theCustomer->ShipAddr->City : '' ),
			     	 'Country'=>  isset($Customer_array['ShipAddr']['Country']) ? $Customer_array['ShipAddr']['Country'] : ((isset($theCustomer->ShipAddr->Country)) ? $theCustomer->ShipAddr->Country : '' ),
			     	 'CountrySubDivisionCode'=>  isset($Customer_array['ShipAddr']['CountrySubDivisionCode']) ? $Customer_array['ShipAddr']['CountrySubDivisionCode'] : ((isset($theCustomer->ShipAddr->CountrySubDivisionCode)) ? $theCustomer->ShipAddr->CountrySubDivisionCode : '' ) ,
			     	 'PostalCode'=>  (isset($Customer_array['ShipAddr']['PostalCode']) && $Customer_array['ShipAddr']['PostalCode'] != '0') ? $Customer_array['ShipAddr']['PostalCode'] : ((isset($theCustomer->ShipAddr->PostalCode)) ? $theCustomer->ShipAddr->PostalCode : '' ),
		 		],
		 		'FullyQualifiedName' => isset($Customer_array['FullyQualifiedName']) ? $Customer_array['FullyQualifiedName'] : ((isset($theCustomer->FullyQualifiedName)) ? $theCustomer->FullyQualifiedName : '' ),
		 		'Notes' => isset($Customer_array['Notes']) ? $Customer_array['Notes'] : ((isset($theCustomer->Notes)) ? $theCustomer->Notes : '' ),
		 		'GivenName' => isset($Customer_array['GivenName']) ? $Customer_array['GivenName'] : ((isset($theCustomer->GivenName)) ? $theCustomer->GivenName : '' ),
		 		'MiddleName' => isset($Customer_array['MiddleName']) ? $Customer_array['MiddleName'] : ((isset($theCustomer->MiddleName)) ? $theCustomer->MiddleName : '' ),
		 		 'CompanyName' => isset($Customer_array['CompanyName']) ? $Customer_array['CompanyName'] : ((isset($theCustomer->CompanyName)) ? $theCustomer->CompanyName : '' ),
			]);
			$resultingCustomerUpdatedObj = $this->dataService->Update($updateCustomer);
			$return = '';
			if ($error) {
				$return['status'] = 0 ; 
				$return['message'] = $error->getResponseBody();
				$return['file_path'] = $filepath;
			}else{
				$return['status'] = 1; 
				$return['message'] = 'success'; 
				$return['data'] = $resultingCustomerUpdatedObj;
				$return['file_path'] = $filepath;
			}
		return  $return ;
	}

	public function Invoice_Add($qb_config,$Invoice_array){
		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));
		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);

		$lineObject = Line::create([
		   "Description" => isset($Invoice_array['Line'][0]['Description']) ? $Invoice_array['Line'][0]['Description'] : '',
		   "Amount" => isset($Invoice_array['Line'][0]['Amount']) ? number_format($Invoice_array['Line'][0]['Amount'],2,'.','') : '',
		   "DetailType" => "SalesItemLineDetail",
		   "SalesItemLineDetail" => [
		   		"TaxInclusiveAmt" => isset($Invoice_array['Line'][0]['Amount']) ? number_format($Invoice_array['Line'][0]['Amount'],2,'.','') : '',
		     	"ItemRef" => [
		       		"value" => isset($Invoice_array['Line'][0]['SalesItemLineDetail']['ItemRef']['value']) ? $Invoice_array['Line'][0]['SalesItemLineDetail']['ItemRef']['value'] : '',
			     ],
			    "Qty" => isset($Invoice_array['Line'][0]['SalesItemLineDetail']['Qty']) ? $Invoice_array['Line'][0]['SalesItemLineDetail']['Qty'] : '',
			    "UnitPrice" => isset($Invoice_array['Line'][0]['SalesItemLineDetail']['UnitPrice']) ? number_format($Invoice_array['Line'][0]['SalesItemLineDetail']['UnitPrice'],2,'.','') : '',
			    "TaxCodeRef" => [
			        	"value" => isset($Invoice_array['Line'][0]['SalesItemLineDetail']['TaxCodeRef']['value']) ? $Invoice_array['Line'][0]['SalesItemLineDetail']['TaxCodeRef']['value'] : '',
			    ],   
		   ]
		]);

		$array1 = [
			"Description" =>  isset($Invoice_array['Line'][1]['Description']) ? $Invoice_array['Line'][1]['Description'] : '',
			"Amount" => isset($Invoice_array['Line'][1]['Amount']) ? - number_format($Invoice_array['Line'][1]['Amount'],2,'.','') : '',
			"DetailType" => "SalesItemLineDetail",
			"SalesItemLineDetail" => [
			   		"TaxInclusiveAmt" => isset($Invoice_array['Line'][1]['Amount']) ? - number_format($Invoice_array['Line'][1]['Amount'],2,'.','') : '',
			     	"ItemRef" => [
			       		"value" => isset($Invoice_array['Line'][1]['SalesItemLineDetail']['ItemRef']['value']) ? $Invoice_array['Line'][1]['SalesItemLineDetail']['ItemRef']['value'] : '',
				     ],
				    "Qty" => 1,
				    "UnitPrice" => isset($Invoice_array['Line'][1]['SalesItemLineDetail']['UnitPrice']) ? - number_format($Invoice_array['Line'][1]['SalesItemLineDetail']['UnitPrice'],2,'.','') : '',
				    "TaxCodeRef" => [
				        "value" =>  isset($Invoice_array['Line'][1]['SalesItemLineDetail']['TaxCodeRef']['value']) ? $Invoice_array['Line'][1]['SalesItemLineDetail']['TaxCodeRef']['value'] : '',
				    ],   
			   ]
		];


		$array2 = [
			"Description" =>  isset($Invoice_array['Line'][2]['Description']) ? $Invoice_array['Line'][2]['Description'] : '',
			"Amount" => isset($Invoice_array['Line'][2]['Amount']) ? - number_format($Invoice_array['Line'][2]['Amount'],2,',','') : '',
			"DetailType" => "SalesItemLineDetail",
			"SalesItemLineDetail" => [
			   		"TaxInclusiveAmt" => isset($Invoice_array['Line'][2]['Amount']) ? - number_format($Invoice_array['Line'][2]['Amount'],2,'.','') : '',
			     	"ItemRef" => [
			       		"value" => isset($Invoice_array['Line'][2]['SalesItemLineDetail']['ItemRef']['value']) ? $Invoice_array['Line'][2]['SalesItemLineDetail']['ItemRef']['value'] : '',
				     ],
				    "Qty" => 1,
				    "UnitPrice" => isset($Invoice_array['Line'][2]['SalesItemLineDetail']['UnitPrice']) ? - number_format($Invoice_array['Line'][2]['SalesItemLineDetail']['UnitPrice'],2,'.','') : '',
				    "TaxCodeRef" => [
				        "value" =>  isset($Invoice_array['Line'][2]['SalesItemLineDetail']['TaxCodeRef']['value']) ? $Invoice_array['Line'][2]['SalesItemLineDetail']['TaxCodeRef']['value'] : '',
				    ],   
			   ]
		];

		$array3 = [
			"Amount" => isset($Invoice_array['DiscountAmt']) ? number_format($Invoice_array['DiscountAmt'],2,'.','') : '',
			"DetailType" => "DiscountLineDetail",
			"DiscountLineDetail" => [
		   		"PercentBased" => false,
		        "DiscountAccountRef" => [
		        	"value" => isset($Invoice_arra['DiscountAccountRef']) ? $Invoice_arra['DiscountAccountRef'] : '',
		        ]   
			]
		];

		$line_array = array();
		if (isset($Invoice_array['Line'][0]['Amount']) && $Invoice_array['Line'][0]['Amount'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject);
		}

		if (isset($Invoice_array['Line'][0]['Amount']) && $Invoice_array['Line'][0]['Amount'] != '0'  && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0'){
			$line_array = array();
			array_push($line_array,$lineObject,$array3);
		}

		if (isset($Invoice_array['Line'][1]['Amount']) && $Invoice_array['Line'][1]['Amount'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array1);
		}

		if (isset($Invoice_array['Line'][2]['Amount']) && $Invoice_array['Line'][2]['Amount'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array2);
		}

		if (isset($Invoice_array['Line'][1]['Amount']) && $Invoice_array['Line'][1]['Amount'] != '0' && isset($Invoice_array['Line'][2]['Amount']) && $Invoice_array['Line'][2]['Amount'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array1,$array2);
		}

		if (isset($Invoice_array['Line'][1]['Amount']) && $Invoice_array['Line'][1]['Amount'] != '0' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array1,$array3);
		}

		if (isset($Invoice_array['Line'][2]['Amount']) && $Invoice_array['Line'][2]['Amount'] != '0' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array2,$array3);
		}

		if (isset($Invoice_array['Line'][1]['Amount']) && $Invoice_array['Line'][1]['Amount'] != '0' && isset($Invoice_array['Line'][2]['Amount']) && $Invoice_array['Line'][2]['Amount'] != '0' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array1,$array2,$array3);
		}


		$theResourceObj = Invoice::create([
				"Line" => $line_array,
				"DocNumber" => isset($Invoice_array['DocNumber']) ? $Invoice_array['DocNumber'] : '',
				"CustomerRef"=> [
				  "value"=> isset($Invoice_array['CustomerRef']['value']) ? $Invoice_array['CustomerRef']['value'] : '',
				],
				'CustomerMemo' => [
				    'value'=> isset($Invoice_array['void_massage']) ? $Invoice_array['void_massage'] : ' '  ,
				],
				'DueDate' =>  isset($Invoice_array['DueDate']) ? $Invoice_array['DueDate'] : '',
				'GlobalTaxCalculation' => isset($Invoice_array['GlobalTaxCalculation']) ? $Invoice_array['GlobalTaxCalculation'] : '',
				'AllowOnlineCreditCardPayment' => isset($Invoice_array['AllowOnlineCreditCardPayment']) ? $Invoice_array['AllowOnlineCreditCardPayment'] : '',
				'ApplyTaxAfterDiscount' => isset($Invoice_array['ApplyTaxAfterDiscount']) ? $Invoice_array['ApplyTaxAfterDiscount'] : '',
				'SalesTermRef' => [
					'value' => sset($Invoice_array['SalesTermRef']) ? $Invoice_array['SalesTermRef'] : '',
				],
				'BillEmail' => [
					'Address' => isset($Invoice_array['BillEmail']['Address']) ? $Invoice_array['BillEmail']['Address'] : ' ',
				],
				'BillAddr' => [
					'PostalCode' => isset($Invoice_array['BillAddr']['PostalCode']) ? $Invoice_array['BillAddr']['PostalCode'] : ' ', 
					'Line1' => isset($Invoice_array['BillAddr']['Line1']) ? $Invoice_array['BillAddr']['Line1'] : ' ', 
					'Line2' => isset($Invoice_array['BillAddr']['Line2']) ? $Invoice_array['BillAddr']['Line2'] : ' ', 
				],
				"TxnTaxDetail" =>  [
				      "TxnTaxCodeRef" => isset($Invoice_array['TxnTaxDetail']['TxnTaxCodeRef']) ? $Invoice_array['TxnTaxDetail']['TxnTaxCodeRef'] : '',
				      "TotalTax" => isset($Invoice_array['TxnTaxDetail']['TotalTax']) ? $Invoice_array['TxnTaxDetail']['TotalTax'] : '',
				      "TaxLine" => [
				         "Amount" =>  isset($Invoice_array['TxnTaxDetail']['TotalTax']) ? $Invoice_array['TxnTaxDetail']['TotalTax'] : '',
				         "DetailType" => "TaxLineDetail",
				         "TaxLineDetail" => [
				            "TaxRateRef" => isset($Invoice_array['TxnTaxDetail']['TaxRateRef']) ? number_format($Invoice_array['TxnTaxDetail']['TaxRateRef'],2,'.','') : '',
				            "PercentBased" => isset($Invoice_array['TxnTaxDetail']['PercentBased']) ? number_format($Invoice_array['TxnTaxDetail']['PercentBased'],2,'.','') : '',
				            "TaxPercent" => isset($Invoice_array['TxnTaxDetail']['TaxPercent']) ? number_format($Invoice_array['TxnTaxDetail']['TaxPercent'],2,'.','') : '',
				            "NetAmountTaxable" => isset($Invoice_array['TxnTaxDetail']['NetAmountTaxable']) ? number_format($Invoice_array['TxnTaxDetail']['NetAmountTaxable'],2,'.','') : '',
				        ]
				    ]
				],
		]);

		$resultingObj = $this->dataService->Add($theResourceObj);
		$error = $this->dataService->getLastError();
		$return = '';
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
		}else{
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['data'] = $resultingObj;
			$return['file_path'] = $filepath;
		}
		return  $return ;
	}

	public function Invoice_Edit($qb_config,$invoice_array){

		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));

		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);

		$entities = $this->dataService->Query("SELECT * FROM Invoice where Id='".$Invoice_array['Id']."'");
		$error = $this->dataService->getLastError();
		$return = '';
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
			return $return;
		}
		if(empty($entities)){
			$return['status'] = 0 ; 
			$return['message'] = 'No data Found';
			$return['file_path'] = $filepath;
			return $return;	
		}
		
		$theInvoice = reset($entities);

		$lineObject = Line::create([
	   	 	 "Description" => (isset($Invoice_array['Line'][0]['Description'])) ? $Invoice_array['Line'][0]['Description'] :  ((isset($theInvoice->Line[0]->Description)) ? $theInvoice->Line[0]->Description : " " ),
	     "Amount" => isset($Invoice_array['Line'][0]['Amount']) ? $Invoice_array['Line'][0]['Amount'] : ((isset($theInvoice->Line[0]->Amount)) ? $theInvoice->Line[0]->Amount : " " ),
	     "DetailType" => "SalesItemLineDetail",
	     "SalesItemLineDetail" => [
	     	"TaxInclusiveAmt" => isset($Invoice_array['Line'][0]['Amount']) ? $Invoice_array['Line'][0]['Amount'] : ((isset($theInvoice->Line[0]->Amount)) ? $theInvoice->Line[0]->Amount : " " ),
	       "ItemRef" => [
	         "value" => isset($Invoice_array['Line'][0]['SalesItemLineDetail']['ItemRef']['value']) ? $Invoice_array['Line'][0]['SalesItemLineDetail']['ItemRef']['value'] : ((isset($theInvoice->Line[0]->SalesItemLineDetail->ItemRef)) ? $theInvoice->Line[0]->SalesItemLineDetail->ItemRef : " " ),
	        ],
	        "Qty" => isset($Invoice_array['Line'][0]['SalesItemLineDetail']['Qty']) ? $Invoice_array['Line'][0]['SalesItemLineDetail']['Qty'] : ((isset($theInvoice->Line[0]->SalesItemLineDetail->Qty)) ? $theInvoice->Line[0]->SalesItemLineDetail->Qty : " " ),
	        "UnitPrice" => isset($Invoice_array['Line'][0]['SalesItemLineDetail']['UnitPrice']) ? $Invoice_array['Line'][0]['SalesItemLineDetail']['UnitPrice'] : ((isset($theInvoice->Line[0]->SalesItemLineDetail->UnitPrice)) ? $theInvoice->Line[0]->SalesItemLineDetail->UnitPrice : " " ),
			"TaxCodeRef" => [
                'value' =>  isset($Invoice_array['Line'][0]['SalesItemLineDetail']['TaxCodeRef']['value']) ? $Invoice_array['Line'][0]['SalesItemLineDetail']['TaxCodeRef']['value'] : ((isset($theInvoice->Line[0]->SalesItemLineDetail->TaxCodeRef->value)) ? $theInvoice->Line[0]->SalesItemLineDetail->TaxCodeRef->value : " " ),
            ],  
	      ],
		]);

		$array1 = [
			 "Description" => isset($Invoice_array['Line'][1]['Description']) ? $Invoice_array['Line'][1]['Description'] : '',
	     "Amount" => isset($Invoice_array['Line'][1]['Amount']) ? - $Invoice_array['Line'][1]['Amount'] : '',
	     "DetailType" => "SalesItemLineDetail",
	     "SalesItemLineDetail" => [
	     	"TaxInclusiveAmt" => isset($Invoice_array['Line'][1]['Amount']) ? - $Invoice_array['Line'][1]['Amount'] : '',
	       "ItemRef" => [
	         "value" => isset($Invoice_array['Line'][1]['ItemRef']) ? - $Invoice_array['Line'][1]['ItemRef'] : '',
	        ],
	        "Qty" => 1,
	        "UnitPrice" => isset($Invoice_array['Line'][1]['SalesItemLineDetail']['UnitPrice']) ? - $Invoice_array['Line'][1]['SalesItemLineDetail']['UnitPrice'] : '',
			"TaxCodeRef" => [
                'value' =>  isset($Invoice_array['Line'][1]['SalesItemLineDetail']['TaxCodeRef']['value']) ? $Invoice_array['Line'][1]['SalesItemLineDetail']['TaxCodeRef']['value'] : ((isset($theInvoice->Line[1]->SalesItemLineDetail->TaxCodeRef->value)) ? $theInvoice->Line[1]->SalesItemLineDetail->TaxCodeRef->value : " " ),
            ],  
	      ],
		];


		$array2 = [
			"Description" => isset($Invoice_array['Line'][2]['Description']) ? $Invoice_array['Line'][2]['Description'] : '',
		     "Amount" => isset($Invoice_array['Line'][2]['Amount']) ? - $Invoice_array['Line'][2]['Amount'] : '',
		     "DetailType" => "SalesItemLineDetail",
		     "SalesItemLineDetail" => [
		     	"TaxInclusiveAmt" => isset($Invoice_array['Line'][2]['Amount']) ? - $Invoice_array['Line'][2]['Amount'] : '',
		       "ItemRef" => [
		         "value" => isset($Invoice_array['Line'][2]['ItemRef']) ? - $Invoice_array['Line'][2]['ItemRef'] : '',
		        ],
		        "Qty" => 1 ,
		        "UnitPrice" => isset($Invoice_array['Line'][2]['SalesItemLineDetail']['UnitPrice']) ? - $Invoice_array['Line'][2]['SalesItemLineDetail']['UnitPrice'] : '',
				"TaxCodeRef" => [
	                'value' => isset($Invoice_array['Line'][2]['SalesItemLineDetail']['TaxCodeRef']['value']) ? $Invoice_array['Line'][2]['SalesItemLineDetail']['TaxCodeRef']['value'] : ((isset($theInvoice->Line[2]->SalesItemLineDetail->TaxCodeRef->value)) ? $theInvoice->Line[2]->SalesItemLineDetail->TaxCodeRef->value : " " ),
	            ],  
		      ],
		];

		$array3 = [
			"Amount" => isset($Invoice_array['DiscountAmt']) ? $Invoice_array['DiscountAmt'] : 0 ,
		     "DetailType" => "DiscountLineDetail",
		     "DiscountLineDetail" => [
		        "PercentBased" => false,
		        "DiscountAccountRef" => [
		        	"value" => isset($Invoice_array['DiscountAccountRef']) ? - $Invoice_array['DiscountAccountRef'] : '',
		        ]
		      ]
		];

		$array4 = [
			"Description" => isset($Invoice_array['Rounding_Des']) ? $Invoice_array['Rounding_Des'] : '' ,
		     "Amount" => isset($Invoice_array['RoundingAmt']) ? $Invoice_array['RoundingAmt'] : ((isset($Invoice_array['RoundingAmt'])) ?  $Invoice_array['RoundingAmt'] : " " ),
		     "DetailType" => "SalesItemLineDetail",
		     "SalesItemLineDetail" => [
		     	"TaxInclusiveAmt" => isset($Invoice_array['RoundingAmt']) ? $Invoice_array['RoundingAmt'] : ((isset($Invoice_array['RoundingAmt'])) ? $Invoice_array['RoundingAmt'] : " " ),
		        "ItemRef" => [
		         "value" => isset($Invoice_array['Rounding']['ItemRef']) ? $Invoice_array$Invoice_array['Rounding']['ItemRef'] : "",
		        ],
		        "Qty" => 1 ,
		        "UnitPrice" => isset($Invoice_array['RoundingAmt']) ? $Invoice_array['RoundingAmt'] : ((isset($Invoice_array['RoundingAmt'])) ? $Invoice_array['RoundingAmt'] : " " ),
				"TaxCodeRef" => [
	                    'value' => isset($Invoice_array['Rounding']['TaxCodeRef']) ? $Invoice_array$Invoice_array['Rounding']['TaxCodeRef'] : "",,
	            ],  
		      ],
		];

		$line_array = array();
		if (isset($Invoice_array['Line'][0]['Amount']) && $Invoice_array['Line'][0]['Amount'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject);
		}

		if (isset($Invoice_array['RoundingAmt']) && $Invoice_array['RoundingAmt'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array4);
		}

		if (isset($Invoice_array['Line'][1]['Amount']) && $Invoice_array['Line'][1]['Amount'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array1);
		}

		if (isset($Invoice_array['Line'][2]['Amount']) && $Invoice_array['Line'][2]['Amount'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array2);
		}

		if (isset($Invoice_array['Line'][1]['Amount']) && $Invoice_array['Line'][1]['Amount'] != '0' && isset($Invoice_array['Line'][2]['Amount']) && $Invoice_array['Line'][2]['Amount'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array1,$array2);
		}

		if (isset($Invoice_array['Line'][1]['Amount']) && $Invoice_array['Line'][1]['Amount'] != '0' && isset($Invoice_array['Line'][2]['Amount']) && $Invoice_array['Line'][2]['Amount'] != '0'  && isset($Invoice_array['RoundingAmt']) && $Invoice_array['RoundingAmt'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array1,$array2,$array4);
		}

		if (isset($Invoice_array['Line'][0]['Amount']) && $Invoice_array['Line'][0]['Amount'] != '0' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array3);
		}


		if (isset($Invoice_array['Line'][0]['Amount']) && $Invoice_array['Line'][0]['Amount'] != '0' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' && isset($Invoice_array['RoundingAmt']) && $Invoice_array['RoundingAmt'] != '0'){
			$line_array = array();
			array_push($line_array,$lineObject,$array3,$array4);
		}

		if (isset($Invoice_array['Line'][1]['Amount']) && $Invoice_array['Line'][1]['Amount'] != '0' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array1,$array3);
		}

		if (isset($Invoice_array['Line'][1]['Amount']) && $Invoice_array['Line'][1]['Amount'] != '0' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' && isset($Invoice_array['RoundingAmt']) && $Invoice_array['RoundingAmt'] != '0'){
			$line_array = array();
			array_push($line_array,$lineObject,$array1,$array3,$array4);
		}

		if (isset($Invoice_array['Line'][2]['Amount']) && $Invoice_array['Line'][2]['Amount'] != '0' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array2,$array3);
		}

		if (isset($Invoice_array['Line'][2]['Amount']) && $Invoice_array['Line'][2]['Amount'] != '0' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' && isset($Invoice_array['RoundingAmt']) && $Invoice_array['RoundingAmt'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array2,$array3,$array4);
		}

		if (isset($Invoice_array['Line'][0]['Amount']) && $Invoice_array['Line'][0]['Amount'] != '0' && isset($Invoice_array['Line'][1]['Amount']) && $Invoice_array['Line'][1]['Amount'] != '0' && isset($Invoice_array['Line'][2]['Amount']) && $Invoice_array['Line'][2]['Amount'] != '0' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array1,$array2,$array3);
		}


		if (isset($Invoice_array['Line'][0]['Amount']) && $Invoice_array['Line'][0]['Amount'] != '0' && isset($Invoice_array['Line'][1]['Amount']) && $Invoice_array['Line'][1]['Amount'] != '0' && isset($Invoice_array['Line'][2]['Amount']) && $Invoice_array['Line'][2]['Amount'] != '0' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0'  && isset($Invoice_array['RoundingAmt']) && $Invoice_array['RoundingAmt'] != '0' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array1,$array2,$array3,$array4);
		}
	

		$theInvoice = reset($entities);
		$updateInvoice = Invoice::update($theInvoice, [
		    'sparse' => 'false',
		    'GlobalTaxCalculation' => 'TaxExcluded',
		    'AllowOnlineCreditCardPayment' => 'false',
		    "ApplyTaxAfterDiscount" => 'true', 
		    'DueDate' =>  isset($Invoice_array['DueDate']) ? $Invoice_array['DueDate'] : ((isset($theInvoice->DueDate)) ? $theInvoice->DueDate : " " ),
		    'CustomerRef' => [
	     	 'value'=> isset($theInvoice->CustomerRef) ? $theInvoice->CustomerRef : " " ,
	 		],
	 		'CustomerMemo' => [
	     	 'value'=>  isset($Invoice_array['void_massage']) ? $Invoice_array['void_massage'] : ((isset($theInvoice->CustomerMemo)) ? $theInvoice->CustomerMemo : " " ),
	 		],
	 		'BillAddr' => [
	 			    'PostalCode' => isset($theInvoice->BillAddr->PostalCode) ? $theInvoice->BillAddr->PostalCode : " ", 
					'Line1' => isset($theInvoice->BillAddr->Line1) ? $theInvoice->BillAddr->Line1 : " ",
					'Line2' => isset($theInvoice->BillAddr->Line2) ? $theInvoice->BillAddr->Line2 : " ",  
			],
			'BillEmail' => [
				'Address' => isset($theInvoice->BillEmail->Address) ? $theInvoice->BillEmail->Address : " ",  
			],
			"TxnTaxDetail" =>  [
			      "TxnTaxCodeRef" => "8",
			      "TotalTax" => isset($Invoice_array['TxnTaxDetail']['TotalTax']) ? $Invoice_array['TxnTaxDetail']['TotalTax'] : ' ',
			      "TaxLine" => [
			         "Amount" => isset($Invoice_array['TxnTaxDetail']['TotalTax']) ? $Invoice_array['TxnTaxDetail']['TotalTax'] : ' ',
			         "DetailType" => "TaxLineDetail",
			         "TaxLineDetail" => [
			            "TaxRateRef" => isset($Invoice_array['TxnTaxDetail']['TaxLine']['TaxLineDetail']['TaxRateRef']) ? $Invoice_array['TxnTaxDetail']['TaxLine']['TaxLineDetail']['TaxRateRef'] : ' ',
			            "PercentBased" => isset($Invoice_array['TxnTaxDetail']['TaxLine']['TaxLineDetail']['PercentBased']) ? $Invoice_array['TxnTaxDetail']['TaxLine']['TaxLineDetail']['PercentBased'] : ' ',
			            "TaxPercent" => isset($Invoice_array['TxnTaxDetail']['TaxLine']['TaxLineDetail']['TaxPercent']) ? $Invoice_array['TxnTaxDetail']['TaxLine']['TaxLineDetail']['TaxPercent'] : ' ',
			            "NetAmountTaxable" => isset($Invoice_array['TxnTaxDetail']['NetAmountTaxable']) ? $Invoice_array['TxnTaxDetail']['NetAmountTaxable'] : ' ',
			        ]
			    ]
			],
	 		"Line" => $line_array,
		]);


					

		$resultingCustomerUpdatedObj = $this->dataService->Update($updateInvoice);
		$error = $this->dataService->getLastError();
		$return = '';
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
		}else{
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['data'] = $resultingCustomerUpdatedObj;
			$return['file_path'] = $filepath;
		}
		return  $return ;
	}

	public function Invoice_Rounding($qb_config,$Invoice_array){

		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));
		
		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);

		$entities = $this->dataService->Query("SELECT * FROM Invoice where Id='".$Invoice_array['Id']."'");
		$error = $this->dataService->getLastError();
		$return = '';
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
			return $return;
		}
		if(empty($entities)){
			$return['status'] = 0 ; 
			$return['message'] = 'No data Found';
			$return['file_path'] = $filepath;
			return $return;	
		}
		   
	    $theInvoice = reset($entities);
		$lineObject = Line::create([
	   	 	 "Description" => isset($theInvoice->Line[0]->Description) ? $theInvoice->Line[0]->Description : " ",
	     "Amount" => isset($theInvoice->Line[0]->Amount) ? $theInvoice->Line[0]->Amount : " ",
	     "DetailType" => "SalesItemLineDetail",
	     "SalesItemLineDetail" => [
	     	"TaxInclusiveAmt" => isset($theInvoice->Line[0]->Amount) ? $theInvoice->Line[0]->Amount : " ",
	       "ItemRef" => [
	         "value" => isset($theInvoice->Line[0]->SalesItemLineDetail->ItemRef) ? $theInvoice->Line[0]->SalesItemLineDetail->ItemRef : " ",
	        ],
	        "Qty" => isset($theInvoice->Line[0]->SalesItemLineDetail->Qty) ? $theInvoice->Line[0]->SalesItemLineDetail->Qty : " ",
	        "UnitPrice" => isset($theInvoice->Line[0]->SalesItemLineDetail->UnitPrice) ? $theInvoice->Line[0]->SalesItemLineDetail->UnitPrice : " ",
			"TaxCodeRef" => [
                'value' => isset($theInvoice->Line[0]->SalesItemLineDetail->TaxCodeRef->value) ? $theInvoice->Line[0]->SalesItemLineDetail->->TaxCodeRef->value : " ",
            ],  
	      ],
		]);

		$array1 = [
   			 "Description" => (isset($theInvoice->Line[1]->DetailType) && $theInvoice->Line[1]->DetailType == 'SalesItemLineDetail' && isset($theInvoice->Line[1]->Description)) ? $theInvoice->Line[1]->Description : " ",
		     "Amount" => (isset($theInvoice->Line[1]->DetailType) && $theInvoice->Line[1]->DetailType == 'SalesItemLineDetail' &&  isset($theInvoice->Line[1]->Amount)) ? $theInvoice->Line[1]->Amount : " ",
		     "DetailType" => "SalesItemLineDetail",
		     "SalesItemLineDetail" => [
		     	"TaxInclusiveAmt" => (isset($theInvoice->Line[1]->DetailType) && $theInvoice->Line[1]->DetailType == 'SalesItemLineDetail' && isset($theInvoice->Line[1]->Amount)) ? $theInvoice->Line[1]->Amount : " ",
		       "ItemRef" => [
		         "value" =>  (isset($theInvoice->Line[1]->DetailType) && $theInvoice->Line[1]->DetailType == 'SalesItemLineDetail' &&  isset($theInvoice->Line[1]->SalesItemLineDetail->ItemRef)) ? $theInvoice->Line[1]->SalesItemLineDetail->ItemRef : " ",
		        ],
		        "Qty" => 1,
		        "UnitPrice" => (isset($theInvoice->Line[1]->DetailType) && $theInvoice->Line[1]->DetailType == 'SalesItemLineDetail' &&  isset($theInvoice->Line[1]->SalesItemLineDetail->UnitPrice)) ? $theInvoice->Line[1]->SalesItemLineDetail->UnitPrice : " " ,
				"TaxCodeRef" => [
	                'value' => (isset($theInvoice->Line[1]->DetailType) && $theInvoice->Line[1]->DetailType == 'SalesItemLineDetail' &&  isset($theInvoice->Line[1]->SalesItemLineDetail->TaxCodeRef->value)) ? $theInvoice->Line[1]->SalesItemLineDetail->TaxCodeRef->value : " " ,
	            ],  
		      ],
		];


		$array2 = [
			"Description" => (isset($theInvoice->Line[2]->DetailType) && $theInvoice->Line[2]->DetailType == 'SalesItemLineDetail' && isset($theInvoice->Line[2]->Description)) ? $theInvoice->Line[2]->Description : " ",
		     "Amount" => (isset($theInvoice->Line[2]->DetailType) && $theInvoice->Line[2]->DetailType == 'SalesItemLineDetail' &&  isset($theInvoice->Line[2]->Amount)) ? $theInvoice->Line[2]->Amount : " ",
		     "DetailType" => "SalesItemLineDetail",
		     "SalesItemLineDetail" => [
		     	"TaxInclusiveAmt" => (isset($theInvoice->Line[2]->DetailType) && $theInvoice->Line[2]->DetailType == 'SalesItemLineDetail' &&  isset($theInvoice->Line[2]->Amount)) ? $theInvoice->Line[2]->Amount : " ",
		       "ItemRef" => [
		         "value" => (isset($theInvoice->Line[2]->DetailType) && $theInvoice->Line[2]->DetailType == 'SalesItemLineDetail' && isset($theInvoice->Line[2]->SalesItemLineDetail->ItemRef)) ? $theInvoice->Line[2]->SalesItemLineDetail->ItemRef : " ",
		        ],
		        "Qty" => 1 ,
		        "UnitPrice" => (isset($theInvoice->Line[2]->DetailType) && $theInvoice->Line[2]->DetailType == 'SalesItemLineDetail' &&  isset($theInvoice->Line[2]->SalesItemLineDetail->UnitPrice)) ? $theInvoice->Line[2]->SalesItemLineDetail->UnitPrice : " " ,
				"TaxCodeRef" => [
	                'value' => (isset($theInvoice->Line[2]->DetailType) && $theInvoice->Line[2]->DetailType == 'SalesItemLineDetail' &&  isset($theInvoice->Line[2]->SalesItemLineDetail->TaxCodeRef->value)) ? $theInvoice->Line[2]->SalesItemLineDetail->TaxCodeRef->value : " " ,
	            ],  
		      ],
		];

		$array3 = [
			"Amount" => isset($Invoice_array['DiscountAmt']) ? $Invoice_array['DiscountAmt'] : 0 ,
		     "DetailType" => "DiscountLineDetail",
		     "DiscountLineDetail" => [
		        "PercentBased" => false,
		        "DiscountAccountRef" => [
		        	"value" => isset($Invoice_array['DiscountAccountRef']) ? $Invoice_array['DiscountAccountRef'] : 0 ,,
		        ]
		      ]
		];

		$array4 = [
			"Description" => 'Invoice Rounding',
		     "Amount" => isset($Invoice_array['RoundingAmt']) ? $Invoice_array['RoundingAmt'] : " ",
		     "DetailType" => "SalesItemLineDetail",
		     "SalesItemLineDetail" => [
		       "TaxInclusiveAmt" => isset($Invoice_array['RoundingAmt']) ? $Invoice_array['RoundingAmt'] : " ",
		       "ItemRef" => [
		         "value" => isset($Invoice_array['ItemRef']) ? $Invoice_array['ItemRef'] : " ",,
		        ],
		        "Qty" => 1 ,
		        "UnitPrice" => isset($Invoice_array['RoundingAmt']) ? $Invoice_array['RoundingAmt'] : '' ,
				"TaxCodeRef" => [
	                'value' => isset($Invoice_array['TaxCodeRef']) ? $Invoice_array['TaxCodeRef'] : '',
	            ],  
		    ],
		];

		$line_array = array();
		if (isset($theInvoice->Line[0]->Amount) && $theInvoice->Line[0]->Amount != '0' && $theInvoice->Line[0]->Amount != '' ){
			$line_array = array();
			array_push($line_array,$lineObject);
		}

		if (isset($Invoice_array['RoundingAmt']) && $Invoice_array['RoundingAmt'] != '0' && $Invoice_array['RoundingAmt'] != '' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array4);
		}

		if (isset($theInvoice->Line[1]->SalesItemLineDetail->ItemRef) && isset($theInvoice->Line[1]->DetailType) && $theInvoice->Line[1]->DetailType == 'SalesItemLineDetail' && ($theInvoice->Line[1]->SalesItemLineDetail->ItemRef == SkillFuture_Credit || $theInvoice->Line[1]->SalesItemLineDetail->ItemRef == SkillFuture_Grant) && isset($theInvoice->Line[1]->Amount) && $theInvoice->Line[1]->Amount != '0' && $theInvoice->Line[1]->Amount != '' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array1);
		}

		if (isset($theInvoice->Line[2]->SalesItemLineDetail->ItemRef) && isset($theInvoice->Line[2]->DetailType) && $theInvoice->Line[2]->DetailType == 'SalesItemLineDetail' && ($theInvoice->Line[2]->SalesItemLineDetail->ItemRef == SkillFuture_Credit || $theInvoice->Line[2]->SalesItemLineDetail->ItemRef == SkillFuture_Grant) && isset($theInvoice->Line[2]->Amount) && $theInvoice->Line[2]->Amount != '0' && $theInvoice->Line[2]->Amount != ''){
			$line_array = array();
			array_push($line_array,$lineObject,$array2);
		}

		if (isset($theInvoice->Line[1]->SalesItemLineDetail->ItemRef) &&  isset($theInvoice->Line[2]->SalesItemLineDetail->ItemRef) &&   isset($theInvoice->Line[1]->DetailType) && $theInvoice->Line[1]->DetailType == 'SalesItemLineDetail' && isset($theInvoice->Line[2]->DetailType) && $theInvoice->Line[2]->DetailType == 'SalesItemLineDetail' && ($theInvoice->Line[1]->SalesItemLineDetail->ItemRef == SkillFuture_Credit || $theInvoice->Line[1]->SalesItemLineDetail->ItemRef == SkillFuture_Grant) &&  ($theInvoice->Line[2]->SalesItemLineDetail->ItemRef == SkillFuture_Credit || $theInvoice->Line[2]->SalesItemLineDetail->ItemRef == SkillFuture_Grant)  && isset($theInvoice->Line[1]->Amount) && $theInvoice->Line[1]->Amount != '0' && $theInvoice->Line[1]->Amount != '' && isset($theInvoice->Line[2]->Amount) && $theInvoice->Line[2]->Amount != '0' && $theInvoice->Line[2]->Amount != '' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array1,$array2);
		}

		if (isset($theInvoice->Line[1]->SalesItemLineDetail->ItemRef) &&  isset($theInvoice->Line[2]->SalesItemLineDetail->ItemRef)  &&isset($theInvoice->Line[1]->DetailType) && $theInvoice->Line[1]->DetailType == 'SalesItemLineDetail' && 
		    isset($theInvoice->Line[2]->DetailType) && $theInvoice->Line[2]->DetailType == 'SalesItemLineDetail' &&
		    ($theInvoice->Line[1]->SalesItemLineDetail->ItemRef == SkillFuture_Credit || $theInvoice->Line[1]->SalesItemLineDetail->ItemRef == SkillFuture_Grant) && ($theInvoice->Line[2]->SalesItemLineDetail->ItemRef == SkillFuture_Credit || $theInvoice->Line[2]->SalesItemLineDetail->ItemRef == SkillFuture_Grant) &&  isset($theInvoice->Line[1]->Amount) && $theInvoice->Line[1]->Amount != '0' && $theInvoice->Line[1]->Amount != '' && isset($theInvoice->Line[2]->Amount) && $theInvoice->Line[2]->Amount != '0' && $theInvoice->Line[2]->Amount != ''  && isset($Invoice_array['RoundingAmt']) && $Invoice_array['RoundingAmt'] != '0' && $Invoice_array['RoundingAmt'] != ''  ){
			$line_array = array();
			array_push($line_array,$lineObject,$array1,$array2,$array4);
		}

		if (isset($theInvoice->Line[0]->Amount) && $theInvoice->Line[0]->Amount != '0' && $theInvoice->Line[0]->Amount != '' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' && $Invoice_array['DiscountAmt'] != ''){
			$line_array = array();
			array_push($line_array,$lineObject,$array3);
		}


		if (isset($theInvoice->Line[0]->Amount) && $theInvoice->Line[0]->Amount != '0' && $theInvoice->Line[0]->Amount != '' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' && $Invoice_array['DiscountAmt'] != '' && isset($Invoice_array['RoundingAmt']) && $Invoice_array['RoundingAmt'] != '0' && $Invoice_array['RoundingAmt'] != ''){
			$line_array = array();
			array_push($line_array,$lineObject,$array3,$array4);
		}

		if (isset($theInvoice->Line[1]->SalesItemLineDetail->ItemRef) && isset($theInvoice->Line[1]->DetailType) && $theInvoice->Line[1]->DetailType == 'SalesItemLineDetail'  && ($theInvoice->Line[1]->SalesItemLineDetail->ItemRef == SkillFuture_Credit || $theInvoice->Line[1]->SalesItemLineDetail->ItemRef == SkillFuture_Grant) && isset($theInvoice->Line[1]->Amount) && $theInvoice->Line[1]->Amount != '0' && $theInvoice->Line[1]->Amount != '' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' && $Invoice_array['DiscountAmt'] != '' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array1,$array3);
		}

		if (isset($theInvoice->Line[1]->SalesItemLineDetail->ItemRef) && isset($theInvoice->Line[1]->DetailType) && $theInvoice->Line[1]->DetailType == 'SalesItemLineDetail' && ($theInvoice->Line[1]->SalesItemLineDetail->ItemRef == SkillFuture_Credit || $theInvoice->Line[1]->SalesItemLineDetail->ItemRef == SkillFuture_Grant) && isset($theInvoice->Line[1]->Amount) && $theInvoice->Line[1]->Amount != '0' && $theInvoice->Line[1]->Amount != '' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' && $Invoice_array['DiscountAmt'] != '' && isset($Invoice_array['RoundingAmt']) && $Invoice_array['RoundingAmt'] != '0' && $Invoice_array['RoundingAmt'] != ''){
			$line_array = array();
			array_push($line_array,$lineObject,$array1,$array3,$array4);
		}

		if (isset($theInvoice->Line[2]->SalesItemLineDetail->ItemRef) && isset($theInvoice->Line[2]->DetailType) && $theInvoice->Line[2]->DetailType == 'SalesItemLineDetail' &&
			($theInvoice->Line[2]->SalesItemLineDetail->ItemRef == SkillFuture_Credit || $theInvoice->Line[2]->SalesItemLineDetail->ItemRef == SkillFuture_Grant) && isset($theInvoice->Line[2]->Amount) && $theInvoice->Line[2]->Amount != '0' && $theInvoice->Line[2]->Amount != '' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0'  && $Invoice_array['DiscountAmt'] != '' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array2,$array3);
		}

		if (isset($theInvoice->Line[2]->SalesItemLineDetail->ItemRef) &&  isset($theInvoice->Line[2]->DetailType) && $theInvoice->Line[2]->DetailType == 'SalesItemLineDetail' && 
			($theInvoice->Line[2]->SalesItemLineDetail->ItemRef == SkillFuture_Credit || $theInvoice->Line[2]->SalesItemLineDetail->ItemRef == SkillFuture_Grant) && isset($theInvoice->Line[2]->Amount) && $theInvoice->Line[2]->Amount != '0' && $theInvoice->Line[2]->Amount != '' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' && $Invoice_array['DiscountAmt'] != '' && isset($Invoice_array['RoundingAmt']) && $Invoice_array['RoundingAmt'] != '0'  && $Invoice_array['RoundingAmt'] != '' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array2,$array3,$array4);
		}

		if (isset($theInvoice->Line[1]->SalesItemLineDetail->ItemRef) &&  isset($theInvoice->Line[2]->SalesItemLineDetail->ItemRef) && 
			($theInvoice->Line[1]->SalesItemLineDetail->ItemRef == SkillFuture_Credit || $theInvoice->Line[1]->SalesItemLineDetail->ItemRef == SkillFuture_Grant) && ($theInvoice->Line[2]->SalesItemLineDetail->ItemRef == SkillFuture_Credit || $theInvoice->Line[2]->SalesItemLineDetail->ItemRef == SkillFuture_Grant) && 
		 isset($theInvoice->Line[1]->DetailType) && $theInvoice->Line[1]->DetailType == 'SalesItemLineDetail' && 
			isset($theInvoice->Line[2]->DetailType) && $theInvoice->Line[2]->DetailType == 'SalesItemLineDetail' && isset($theInvoice->Line[0]->Amount) && $theInvoice->Line[0]->Amount != '0' && $theInvoice->Line[0]->Amount != '' && isset($theInvoice->Line[1]->Amount) && $theInvoice->Line[1]->Amount != '0' && $theInvoice->Line[1]->Amount != '' && isset($theInvoice->Line[2]->Amount) && $theInvoice->Line[2]->Amount != '0' && $theInvoice->Line[2]->Amount != '0' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' && $Invoice_array['DiscountAmt'] != '' ){
			$line_array = array();
			array_push($line_array,$lineObject,$array1,$array2,$array3);
		}


		if (isset($theInvoice->Line[1]->SalesItemLineDetail->ItemRef) &&  isset($theInvoice->Line[2]->SalesItemLineDetail->ItemRef) && 
			($theInvoice->Line[1]->SalesItemLineDetail->ItemRef == SkillFuture_Credit || $theInvoice->Line[1]->SalesItemLineDetail->ItemRef == SkillFuture_Grant) && ($theInvoice->Line[2]->SalesItemLineDetail->ItemRef == SkillFuture_Credit || $theInvoice->Line[2]->SalesItemLineDetail->ItemRef == SkillFuture_Grant) && isset($theInvoice->Line[1]->DetailType) && $theInvoice->Line[1]->DetailType == 'SalesItemLineDetail' && 
			isset($theInvoice->Line[2]->DetailType) && $theInvoice->Line[2]->DetailType == 'SalesItemLineDetail' && isset($theInvoice->Line[0]->Amount) && $theInvoice->Line[0]->Amount != '0' && $theInvoice->Line[0]->Amount != '' && isset($theInvoice->Line[1]->Amount) && $theInvoice->Line[1]->Amount != '0' && $theInvoice->Line[1]->Amount != '' && isset($theInvoice->Line[2]->Amount) && $theInvoice->Line[2]->Amount != '0' && $theInvoice->Line[2]->Amount != '0' && isset($Invoice_array['DiscountAmt']) && $Invoice_array['DiscountAmt'] != '0' && $Invoice_array['DiscountAmt'] != '' && isset($Invoice_array['RoundingAmt']) && $Invoice_array['RoundingAmt'] != '0' && $Invoice_array['RoundingAmt'] != ''){
			$line_array = array();
			array_push($line_array,$lineObject,$array1,$array2,$array3,$array4);
		}


		$theInvoice = reset($entities);
		$updateInvoice = Invoice::update($theInvoice, [
		    'sparse' => 'false',
		    'GlobalTaxCalculation' => 'TaxExcluded',
		    'AllowOnlineCreditCardPayment' => 'false',
		    "ApplyTaxAfterDiscount" => 'true', 
		    'CustomerRef' => [
	     	 'value'=> isset($theInvoice->CustomerRef) ? $theInvoice->CustomerRef : " " ,
	 		],
	 		'BillAddr' => [
	 			    'PostalCode' => isset($theInvoice->BillAddr->PostalCode) ? $theInvoice->BillAddr->PostalCode : " ", 
					'Line1' => isset($theInvoice->BillAddr->Line1) ? $theInvoice->BillAddr->Line1 : " ",
					'Line2' => isset($theInvoice->BillAddr->Line2) ? $theInvoice->BillAddr->Line2 : " ",  
			],
			'BillEmail' => [
				'Address' => isset($theInvoice->BillEmail->Address) ? $theInvoice->BillEmail->Address : " ",  
			],
			"TxnTaxDetail" =>  [
			      "TxnTaxCodeRef" => "8",
			      "TotalTax" => isset($Invoice_array['TxnTaxDetail']['TotalTax']) ? $Invoice_array['TxnTaxDetail']['TotalTax'] : ' ',
			      "TaxLine" => [
			         "Amount" => isset($Invoice_array['TxnTaxDetail']['TotalTax']) ? $Invoice_array['TxnTaxDetail']['TotalTax'] : ' ',
			         "DetailType" => "TaxLineDetail",
			         "TaxLineDetail" => [
			            "TaxRateRef" => isset($Invoice_array['TxnTaxDetail']['TaxRateRef']) ? $Invoice_array['TxnTaxDetail']['TaxRateRef'] : ' ',
			            "PercentBased" => "true",
			            "TaxPercent" => isset($Invoice_array['TxnTaxDetail']['TaxPercent']) ? $Invoice_array['TxnTaxDetail']['TaxPercent'] : ' ',
			            "NetAmountTaxable" => isset($Invoice_array['TxnTaxDetail']['NetAmountTaxable']) ? $Invoice_array['TxnTaxDetail']['NetAmountTaxable'] : ' ',
			        ]
			    ]
			],
	 		"Line" => $line_array,
		]);

					
		$resultingCustomerUpdatedObj = $this->dataService->Update($updateInvoice);
		$error = $this->dataService->getLastError();
		$return = '';
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
		}else{
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['data'] = $resultingCustomerUpdatedObj;
			$return['file_path'] = $filepath;
		}
		return  $return ;
	}

	public function Payment_Add($qb_config,$Payment_array){

		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));
		
		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);

		$theResourceObj = Payment::create([
			"TotalAmt" => isset($Payment_array['TotalAmt']) ? $Payment_array['TotalAmt'] : '',
			'PaymentMethodRef' => [
				"value" => isset($Payment_array['PaymentMethodRef']) ? $Payment_array['PaymentMethodRef'] : '',
			],
			"ProcessPayment" => true,
			"CustomerRef"=> [
			  "value"=> isset($Payment_array['CustomerRef']['value']) ? $Payment_array['CustomerRef']['value'] : '',
			],
			"TxnDate" =>  isset($Payment_array['TxnDate']) ? $Payment_array['TxnDate'] : '',
			"PrivateNote" => isset($Payment_array['PrivateNote']) ? $Payment_array['PrivateNote'] : '',
			"PaymentRefNum" => isset($Payment_array['PaymentRefNum']) ? $Payment_array['PaymentRefNum'] : '',
		]);

	
		$resultingObj = $this->dataService->Add($theResourceObj);
		$error = $this->dataService->getLastError();
		$return = '';
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
		}else{
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['data'] = $resultingObj;
			$return['file_path'] = $filepath;
		}
		return  $return ;
	}

	public function Payment_Edit($qb_config,$Payment_array){

		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));

		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);

		$entities = $this->dataService->Query("SELECT * FROM Payment where Id='".$Payment_array['Id']."'");
		$error = $this->dataService->getLastError();
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
			return $return;
		}
		if(empty($entities)){
			$return['status'] = 0 ; 
			$return['message'] = 'No Data Found';
			$return['file_path'] = $filepath;
			return $return;	
		}
		$thePayment = reset($entities);
		$theResourceObj = Payment::update($thePayment,[
			"TotalAmt" => isset($Payment_array['TotalAmt']) ? $Payment_array['TotalAmt'] : ((isset($thePayment->TotalAmt))  ? $thePayment->TotalAmt : " "),
			"Id" =>  isset($Payment_array['Id']) ? $Payment_array['Id'] : '',
			"ProcessPayment" =>  isset($Payment_array['ProcessPayment']) ? $Payment_array['ProcessPayment'] : ((isset($thePayment->ProcessPayment))  ? $thePayment->ProcessPayment : " "),
			"ProcessPayment" => true,
			"Line" => [
				[
					"Amount" => isset($Payment_array['Line'][0]['Amount']) ? $Payment_array['Line'][0]['Amount'] : '',
					"LinkedTxn" => [
						"TxnId" => isset($Payment_array['Line'][0]['LinkedTxn'][0]['TxnId']) ? $Payment_array['Line'][0]['LinkedTxn'][0]['TxnId'] : '',
						"TxnType" => 'Invoice',
					],
				],
			],
			"CustomerRef"=> [
			  "value"=> isset($Payment_array['CustomerRef']['value']) ? $Payment_array['CustomerRef']['value'] : '',
			],
			"TxnDate" =>  isset($Payment_array['TxnDate']) ? $Payment_array['TxnDate'] : date('Y-m-d'),
			"Id" => isset($Payment_array['Id']) ? $Payment_array['Id'] : '',
		]);
		$resultingObj = $this->dataService->Add($theResourceObj);
		$error = $this->dataService->getLastError();
		$return = '';
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
		}else{
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['data'] = $resultingObj;
			$return['file_path'] = $filepath;
		}
		return  $return ;
	}

	public function Payment_Edit_with_creditnote($qb_config,$Payment_array){

		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));

		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);

	
		$entities = $this->dataService->Query("SELECT * FROM Payment where Id='".$Payment_array['Id']."'");
		$error = $this->dataService->getLastError();
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
			return $return;
		}
		if(empty($entities)){
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
			return $return;	
		}
		$thePayment = reset($entities);

		$line_array = array();
			
		$lineObject = Line::create([
	   	 		"Amount" => isset($Payment_array['Line'][0]['Amount']) ? $Payment_array['Line'][0]['Amount'] : '',
			"LinkedTxn" => [
				"TxnId" => isset($Payment_array['Line'][0]['LinkedTxn'][0]['TxnId']) ? $Payment_array['Line'][0]['LinkedTxn'][0]['TxnId'] : '',
				"TxnType" => 'Invoice',
			],
		]);
		
		$CreditNote_array = array();
		$i = 0 ;
		foreach ($Payment_array['Line'] as $key => $value) {
			if ($key != 0 ) {
				$CreditNote_array[$i] = [
						"Amount" => isset($value[$i]['Amount']) ? $value[$i]['Amount'] : '',
					    "LinkedTxn" => [
						"TxnId" => isset($value[$i]['LinkedTxn'][0]['TxnId']) ? $value[$i]['LinkedTxn'][0]['TxnId'] : '',
						"TxnType" => 'CreditMemo',
					],
					];
				array_push($line_array,$lineObject,$CreditNote_array[$i]);
				$i++;
			}
		}
		$theResourceObj = Payment::update($thePayment,[
			"SyncToken" => $thePayment->SyncToken,
			"sparse" => 'false',
			"TotalAmt" =>  isset($Payment_array['TotalAmt']) ? $Payment_array['TotalAmt'] : '',
			"ProcessPayment" =>  isset($Payment_array['TotalAmt']) ? $Payment_array['TotalAmt'] : '',
			"ProcessPayment" => true,
			'TxnDate' => isset($Payment_array['TxnDate']) ? $Payment_array['TxnDate'] : '',
			"Id" =>  isset($Payment_array['Id']) ? $Payment_array['Id'] : '',
			"Line" => $line_array,
			"CustomerRef"=> [
			  "value"=> isset($Payment_array['CustomerRef']['value']) ? $Payment_array['CustomerRef']['value'] : '',
			],
			"Id" => isset($Payment_array['Id']) ? $Payment_array['Id'] : '',
		]);


		$resultingObj = $this->dataService->Add($theResourceObj);
		$error = $this->dataService->getLastError();
		
		$return = '';
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
		}else{
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['data'] = $resultingObj;
			$return['file_path'] = $filepath;
		}
		return  $return ;
	}

	public function Item_Add($qb_config,$Item_array){

		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));

		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);


		$dateTime = new DateTime('NOW');
		$Item = Item::create([
		      "Name" => isset($Item_array['Name']) ? $Item_array['Name'] : '',
		      "Description" => isset($Item_array['Description']) ? $Item_array['Description'] : '',
		      "Active" => true,
		      "Type" => "NonInventory",
		      "IncomeAccountRef"=> [
		         "value"=> isset($Item_array['IncomeAccountRef']) ? $Item_array['IncomeAccountRef'] : '',
		      ],
		      "InvStartDate"=> $dateTime
		]);
		$resultingObj = $this->dataService->Add($Item);
		$error = $this->dataService->getLastError();
		$return = array();
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
		}else{
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['data'] = $resultingObj;
			$return['file_path'] = $filepath;
		}
		return  $return ;
	}

	public function Item_Edit($qb_config,$Item_array){

		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));

		$filepath = $this->dataService->setLogLocation($quickbook_log_dir);

		$entities = $this->dataService->Query("SELECT * FROM Item where Id='".$Item_array['Id']."'");
		$error = $this->dataService->getLastError();
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
			return $return;
		}
		if(empty($entities)){
			$return['status'] = 0 ; 
			$return['message'] = 'No Data Found';
			$return['file_path'] = $filepath;
			return $return;	
		}
		$theItem = reset($entities);
		$updateItem = Item::update($theItem, [
		    'sparse' => 'false',
		    'Name' => isset($Item_array['Name']) ? $Item_array['Name'] : '',
		    'Description' => isset($Item_array['Description']) ? $Item_array['Description'] : '',
	 			'Active' => true,
		      'Type' => 'NonInventory',
		      'IncomeAccountRef'=> [
		         'value'=> isset($Item_array['IncomeAccountRef']) ? $Item_array['IncomeAccountRef'] : '',
		      ]
		]);
		$resultingItemUpdatedObj = $this->dataService->Update($updateItem);
		$error = $this->dataService->getLastError();
		$return = '';
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
		}else{
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['data'] = $resultingItemUpdatedObj;
			$return['file_path'] = $filepath;
		}
		return  $return ;
	}

	public function CreditNote_Add($qb_config,$CreditNote_array){

		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));
		
		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);

		$theResourceObj = CreditMemo::create([
		     "Line" => [
		   [
		     "Amount" => isset($CreditNote_array['Line'][0]['Amount']) ? $CreditNote_array['Line'][0]['Amount'] : '',
		     "Description" => isset($CreditNote_array['Line'][0]['Description']) ? $CreditNote_array['Line'][0]['Description'] : '',
		     "DetailType" => "SalesItemLineDetail",
		     "SalesItemLineDetail" => [
		       "ItemRef" => [
		         "value" => isset($CreditNote_array['Line'][0]['ItemRef']) ? $CreditNote_array['Line'][0]['ItemRef'] : '',
		        ],
		        "Qty" => 1 ,
		        "TaxCodeRef" => [
		        	"value" => isset($CreditNote_array['Line'][0]['TaxCodeRef']) ? $CreditNote_array['Line'][0]['TaxCodeRef'] : '',
		    	]
		      ]
		    ],
		"DocNumber" => isset($CreditNote_array['DocNumber']) ? $CreditNote_array['DocNumber'] : '', 
		"CustomerRef"=> [
		  "value"=> isset($CreditNote_array['CustomerRef']['value']) ? $CreditNote_array['CustomerRef']['value'] : '',
		],
		],
		]);
		$resultingObj = $this->dataService->Add($theResourceObj);
		$error = $this->dataService->getLastError();
		$return = '';
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
		}else{
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['data'] = $resultingObj;
			$return['file_path'] = $filepath;
		}
		return  $return ;
	}

	public function Void_invoice($qb_config,$Invoice_array){

		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));
		
		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);


		$entities = $this->dataService->Query("SELECT * FROM Invoice where Id='".$Invoice_array['Id']."'");
		$theInvoice = reset($entities);
		$error = $this->dataService->getLastError();
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
			return $return;
		}
		$SyncToken = $theInvoice->SyncToken;
		$theResourceObj = Invoice::create([
		  "SyncToken" => $SyncToken,
		  "Id" => $Invoice_array['Id'],
		  "PrivateNote" => isset($Invoice_array['void_massage']) ? $theInvoice->PrivateNote."\n"." Void Reason :- ".$Invoice_array['void_massage'] : '',
		  "sparse" => true
		]);
		$resultingObj = $this->dataService->Void($theResourceObj);
		$error = $this->dataService->getLastError();
		$return = '';
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
		}else{
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['data'] = $resultingObj;
			$return['file_path'] = $filepath;
		} 
		return  $return ;
	}

	public function Void_creditnote($qb_config,$CreditNote_array){

		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($config_setting) ? $config_setting['auth_mode'] : '',
		  'ClientID' => isset($config_setting) ? $config_setting['ClientID'] : '',
		  'ClientSecret' => isset($config_setting) ? $config_setting['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($config_setting) ? $config_setting['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($config_setting) ? $config_setting['refreshTokenKey'] : '',
		  'QBORealmID' => isset($config_setting) ? $config_setting['QBORealmID'] : '',
		  'baseUrl' => isset($config_setting) ? $config_setting['baseUrl'] : ''
		));
		
		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);

		$entities = $this->dataService->Query("SELECT * FROM CreditMemo where Id='".$CreditNote_array['Id']."'");
		$theCreditNote = reset($entities);
		$error = $this->dataService->getLastError();
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
			return $return;
		}
		$SyncToken = $theCreditNote->SyncToken;
		$theResourceObj = CreditMemo::create([
		  "SyncToken" => $SyncToken,
		  "Id" => $CreditNote_array['Id'],
		  "PrivateNote" => isset($Invoice_array['void_massage']) ? $theCreditNote->PrivateNote."\n"." Void Reason :- ".$CreditNote_array['void_massage'] : '',
		  "sparse" => true
		]);
		$resultingObj = $this->dataService->Delete($theResourceObj);
		$error = $this->dataService->getLastError();
		$return = '';
		if ($error) {
			$return['status'] = 0 ; 
			$return['file_path'] = $filepath;
			$return['message'] = $error->getResponseBody();
		}else{
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['data'] = $resultingObj;
			$return['file_path'] = $filepath;
		} 
		return  $return ;
	}


	public function Void_payment($qb_config,$Invoice_array){

		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));

		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);

		$entities = $this->dataService->Query("SELECT * FROM Payment where Id='".$Invoice_array['Id']."'");
		$thePayment = reset($entities);
		$error = $this->dataService->getLastError();
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
			return $return;
		}
		$SyncToken = $thePayment->SyncToken;
		$theResourceObj = Payment::create([
		  "SyncToken" => $SyncToken,
		  "Id" => $Invoice_array['Id'],
	 	  "PrivateNote" => isset($Invoice_array['void_massage']) ? $thePayment->PrivateNote."\n"." Void Reason :- ".$Invoice_array['void_massage'] : '',
		  "sparse" => true
		]);

		$resultingObj = $this->dataService->Void_Payment($theResourceObj);
		$error = $this->dataService->getLastError();
		$return = '';
		if ($resultingObj) {
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['data'] = $resultingObj;
			$return['file_path'] = $filepath;
		} else {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
		}
		return  $return ;
	}

	public function Refund_invoice_Add($qb_config,$Refund_array){

		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));
		
		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);

		$theResourceObj = RefundReceipt::create([
		     "Line" => [
		   [
		     "Amount" => isset($Refund_array['Amount']) ? $Refund_array['Amount'] : '',
		     "Description" => isset($Refund_array['Description']) ? $Refund_array['Description'] : '',
		     "DetailType" => "SalesItemLineDetail",
		     "SalesItemLineDetail" => [
		       "ItemRef" => [
		         "value" =>  isset($Refund_array['ItemRef']) ? $Refund_array['ItemRef'] : '' ,
		        ],
		         "TaxCodeRef" => [
		        	"value" => isset($Refund_array['TaxCodeRef']) ? $Refund_array['TaxCodeRef'] : '' , /// No Tax
		        ],		      
		      	],
		      ]
		    ],
		"DepositToAccountRef"=> [
		  "value"=> isset($Refund_array['DepositToAccountRef']) ? $Refund_array['DepositToAccountRef'] : '' ,          /////// Other Current Asset or Bank 
		],
		 "DocNumber" => isset($Refund_array['DocNumber']) ? $Refund_array['DocNumber'] : '',
		 "CustomerRef" => [
		    "value" => isset($Refund_array['CustomerRef']) ? $Refund_array['CustomerRef'] : '',
		 ],
		]);
		$resultingObj = $this->dataService->Add($theResourceObj);
		$error = $this->dataService->getLastError();
		$return = '';
		if ($error) {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
		}else{
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['data'] = $resultingObj;
			$return['file_path'] = $filepath;
		}
		return  $return ;
	}

	public function Payment_method_Add($qb_config,$Paymethod_array){

		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));
		
		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);


		$theResourceObj = PaymentMethod::create([
		  "SyncToken" => 0,
	 	  "Name" => isset($Paymethod_array['name']) ? $Paymethod_array['name'] : '',
		  "sparse" => false,
		  "Active" => true
		]);

		$resultingObj = $this->dataService->Add($theResourceObj);

		$error = $this->dataService->getLastError();
		$return = array();
		if ($resultingObj) {
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['data'] = $resultingObj;
			$return['file_path'] = $filepath;
		} else {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
		}
		return  $return ;
	}

	public function Term_Add($qb_config,$Term_array){

		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));

		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);

		$theResourceObj = Term::create([
		  "SyncToken" => 0,
	 	  "DueDays" => isset($Term_array['duedays']) ? $Term_array['duedays'] : '',
	 	  "Name" => isset($Term_array['name']) ? $Term_array['name'] : '',
		  "sparse" => false,
		  "Active" => true
		]);

		$resultingObj = $this->dataService->Add($theResourceObj);

		$error = $this->dataService->getLastError();
		$return = array();
		if ($resultingObj) {
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['data'] = $resultingObj;
			$return['file_path'] = $filepath;
		} else {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
		}
		return  $return ;
	}

	public function TaxService_Add($qb_config,$Tax_array){

		$this->dataService = DataService::Configure(array(
		  'auth_mode' => isset($qb_config) ? $qb_config['auth_mode'] : '',
		  'ClientID' => isset($qb_config) ? $qb_config['ClientID'] : '',
		  'ClientSecret' => isset($qb_config) ? $qb_config['ClientSecret'] : '',
		  'accessTokenKey' =>  isset($qb_config) ? $qb_config['accessTokenKey'] : '',
		  'refreshTokenKey' => isset($qb_config) ? $qb_config['refreshTokenKey'] : '',
		  'QBORealmID' => isset($qb_config) ? $qb_config['QBORealmID'] : '',
		  'baseUrl' => isset($qb_config) ? $qb_config['baseUrl'] : ''
		));

		$filepath =  $this->dataService->setLogLocation($quickbook_log_dir);


		$TaxRateDetails = array();
	    $currentTaxServiceDetail = TaxRate::create([
	     "TaxRateName" =>  isset($Tax_array['name']) ? $Tax_array['name'] : '',
	     "RateValue" =>  isset($Tax_array['tax_rate']) ? $Tax_array['tax_rate'] : '' ,
	     "TaxAgencyId" => "1",
	     "TaxApplicableOn" => "Sales"
	    ]);
	    $TaxRateDetails[] = $currentTaxServiceDetail;

		$theResourceObj = TaxService::create([
		  "TaxCode" =>  isset($Tax_array['name']) ? $Tax_array['name'] : '',
		  "TaxRateDetails" => $TaxRateDetails
		]);

		$resultingObj = $this->dataService->Add($theResourceObj);

		$error = $this->dataService->getLastError();
		$return = array();
		if ($resultingObj) {
			$return['status'] = 1; 
			$return['message'] = 'success'; 
			$return['file_path'] = $filepath;
			$return['data'] = $resultingObj;
		} else {
			$return['status'] = 0 ; 
			$return['message'] = $error->getResponseBody();
			$return['file_path'] = $filepath;
		}
		return  $return ;
	}

}


?>