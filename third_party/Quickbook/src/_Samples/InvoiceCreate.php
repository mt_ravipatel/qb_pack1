<?php
//Replace the line with require "vendor/autoload.php" if you are using the Samples from outside of _Samples folder
include('../config.php');

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Invoice;

// Prep Data Services
$dataService = DataService::Configure(array(
       'auth_mode' => 'oauth2',
         'ClientID' => "ABLc7kNZB8gYcRGyfh0grLa3Gwo22QHp3SvqHiSqyk2W4vGshA",
         'ClientSecret' => "8QND8qYVwXh9t9zi9GFFmdLUjDiWG1O5MXZtprxb",
         'accessTokenKey' =>  "eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..Pk0YXOLdrivqlwkhE-qSOA.3QNc1W2HJ68s2f5xGLhJ2IqPSEwmXA0DJn0hkWiEUDkPKSzjPfxd7DMwFEof-P9FmKossow9gSPCcleQIRinPEdJMWizVWHktDW7yobDn9R490SbEIUTS6t1J86G8PKSkK4QCaDpS0s4LkpxE6oQqPvM0t3L9ddlR-ilVmWYqxyjtIQnntnvSCK1Ruj1-7iuhCR71HKXQ0xhkzr3BP5a0eauyjCPHjJxkE5su2C518QyX_KhAFz6XGIx5VNmFOl9kH5n5j_TGEdm1XIvMIcJ5KXs2QIPRDuGNWPOY2isvknixOTOSDpExYDGZo1rJbOHc7e9aNJeg4fylvEO1zQk4e5efa8s6i1WEn4zjX97W7uxbOBKvdy4oSTcKbqtd2surtw95BVKOZftzQL832gNOBg8FNqzKgeGlvRI3gybIHKilRz3x7lK4tmkkr2m4yVRCebNMIN4jVUOammNGUk46bvA4pBbW4nEdn_Ag1GjiwYqogF3HRfocx57qP4pJUbgz5U0Xmm_gBaY90NKl-mEALm85L79_aw4bw9Lbe7Om2W12JUT55ohMi_gGhK4gnTZLzFtEi9rXvKD_juqT2MUlkt4h7okM3yZPqa_mWBbrVH5DMBMHtjLxK7VCvkc1GDLu39yF2rKwj17qVrZdpfNe0d3ZyQ9qM-aWCDLl9jOBgovQw4iq1qWVTQOL5cR3NJdD8zSN-Nu10lqJUuhI_x50GQakNcxOcZYf2K4qHCU4Eg.05KPjmH0fCpzVd1szM2FYg",
         'refreshTokenKey' => 'AB115713082268VNrw9rqyaKZ56C37hew5EnCutwex49YU9lsx',
         'QBORealmID' => "4620816365002625470",
         'baseUrl' => "Development"
));
$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

$dataService->throwExceptionOnError(true);
//Add a new Invoice
$theResourceObj = Invoice::create([
     "Line" => [
   [
     "Amount" => 100.00,
     "DetailType" => "SalesItemLineDetail",
     "SalesItemLineDetail" => [
       "ItemRef" => [
         "value" => 2,
         "name" => "Hours"
        ]
      ]
      ]
    ],
"CustomerRef"=> [
  "value"=> 1
],
      "BillEmail" => [
            "Address" => "Familiystore@intuit.com"
      ],
      "BillEmailCc" => [
            "Address" => "a@intuit.com"
      ],
      "BillEmailBcc" => [
            "Address" => "v@intuit.com"
      ]
]);
$resultingObj = $dataService->Add($theResourceObj);


$error = $dataService->getLastError();
if ($error) {
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
}
else {
    echo "Created Id={$resultingObj->Id}. Reconstructed response body:\n\n";
    $xmlBody = XmlObjectSerializer::getPostXmlFromArbitraryEntity($resultingObj, $urlResource);
    echo $xmlBody . "\n";
}

/*
Created Customer Id=801. Reconstructed response body:

<?xml version="1.0" encoding="UTF-8"?>
<ns0:Customer xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:Id>801</ns0:Id>
  <ns0:SyncToken>0</ns0:SyncToken>
  <ns0:MetaData>
    <ns0:CreateTime>2013-08-05T07:41:45-07:00</ns0:CreateTime>
    <ns0:LastUpdatedTime>2013-08-05T07:41:45-07:00</ns0:LastUpdatedTime>
  </ns0:MetaData>
  <ns0:GivenName>GivenName21574516</ns0:GivenName>
  <ns0:FullyQualifiedName>GivenName21574516</ns0:FullyQualifiedName>
  <ns0:CompanyName>CompanyName426009111</ns0:CompanyName>
  <ns0:DisplayName>GivenName21574516</ns0:DisplayName>
  <ns0:PrintOnCheckName>CompanyName426009111</ns0:PrintOnCheckName>
  <ns0:Active>true</ns0:Active>
  <ns0:Taxable>true</ns0:Taxable>
  <ns0:Job>false</ns0:Job>
  <ns0:BillWithParent>false</ns0:BillWithParent>
  <ns0:Balance>0</ns0:Balance>
  <ns0:BalanceWithJobs>0</ns0:BalanceWithJobs>
  <ns0:PreferredDeliveryMethod>Print</ns0:PreferredDeliveryMethod>
</ns0:Customer>
*/
