<?php
//Replace the line with require "vendor/autoload.php" if you are using the Samples from outside of _Samples folder
include('../config.php');

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;


$dataService = DataService::Configure(array(
  'auth_mode' => 'oauth2',
  'ClientID' => "ABLc7kNZB8gYcRGyfh0grLa3Gwo22QHp3SvqHiSqyk2W4vGshA",
  'ClientSecret' => "8QND8qYVwXh9t9zi9GFFmdLUjDiWG1O5MXZtprxb",
  'accessTokenKey' =>  'eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..Pk0YXOLdrivqlwkhE-qSOA.3QNc1W2HJ68s2f5xGLhJ2IqPSEwmXA0DJn0hkWiEUDkPKSzjPfxd7DMwFEof-P9FmKossow9gSPCcleQIRinPEdJMWizVWHktDW7yobDn9R490SbEIUTS6t1J86G8PKSkK4QCaDpS0s4LkpxE6oQqPvM0t3L9ddlR-ilVmWYqxyjtIQnntnvSCK1Ruj1-7iuhCR71HKXQ0xhkzr3BP5a0eauyjCPHjJxkE5su2C518QyX_KhAFz6XGIx5VNmFOl9kH5n5j_TGEdm1XIvMIcJ5KXs2QIPRDuGNWPOY2isvknixOTOSDpExYDGZo1rJbOHc7e9aNJeg4fylvEO1zQk4e5efa8s6i1WEn4zjX97W7uxbOBKvdy4oSTcKbqtd2surtw95BVKOZftzQL832gNOBg8FNqzKgeGlvRI3gybIHKilRz3x7lK4tmkkr2m4yVRCebNMIN4jVUOammNGUk46bvA4pBbW4nEdn_Ag1GjiwYqogF3HRfocx57qP4pJUbgz5U0Xmm_gBaY90NKl-mEALm85L79_aw4bw9Lbe7Om2W12JUT55ohMi_gGhK4gnTZLzFtEi9rXvKD_juqT2MUlkt4h7okM3yZPqa_mWBbrVH5DMBMHtjLxK7VCvkc1GDLu39yF2rKwj17qVrZdpfNe0d3ZyQ9qM-aWCDLl9jOBgovQw4iq1qWVTQOL5cR3NJdD8zSN-Nu10lqJUuhI_x50GQakNcxOcZYf2K4qHCU4Eg.05KPjmH0fCpzVd1szM2FYg',
  'refreshTokenKey' => "AB115713082268VNrw9rqyaKZ56C37hew5EnCutwex49YU9lsx",
  'QBORealmID' => "4620816365002625470",
  'baseUrl' => "Development"
));

$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");


$OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();

$accessToken = $OAuth2LoginHelper->refreshToken();
$error = $OAuth2LoginHelper->getLastError();
if ($error) {
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
    return;
}
$dataService->updateOAuth2Token($accessToken);


$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

// Iterate through all Accounts, even if it takes multiple pages
$i = 1;
while (1) {
    $allAccounts = $dataService->FindAll('Account', $i, 500);
    $error = $dataService->getLastError();
    if ($error) {
        echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
        echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
        echo "The Response message is: " . $error->getResponseBody() . "\n";
        exit();
    }

    if (!$allAccounts || (0==count($allAccounts))) {
        break;
    }

    foreach ($allAccounts as $oneAccount) {
        echo "Account[".($i++)."]: {$oneAccount->Name}\n";
        echo "\t * Id: [{$oneAccount->Id}]\n";
        echo "\t * AccountType: [{$oneAccount->AccountType}]\n";
        echo "\t * AccountSubType: [{$oneAccount->AccountSubType}]\n";
        echo "\t * Active: [{$oneAccount->Active}]\n";
        echo "\n";
    }
}

/*

Example output:

Account[0]: Travel Meals
     * Id: NG:42315
     * AccountType: Expense
     * AccountSubType:

Account[1]: COGs
     * Id: NG:40450
     * AccountType: Cost of Goods Sold
     * AccountSubType:

...

*/
 ?>
