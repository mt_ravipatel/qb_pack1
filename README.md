

## README ##

### Quickbook Integretion Step  ###


## Step 01 ##

=> You need to run This sql to run QuickBook integration.
  -> qb_config.sql

## Step 02 ##

=> Create Qb Account from url - https://quickbooks.intuit.com/sg/pricing/

## Step 03 ##

=> After Create QB Account  open url : https://developer.intuit.com.

## Step 04 ##

=> For Create an App 

## Step 05 ##

=> After Created app  select menu app -> Keys & OAuth.

## Step 06 ##

=> Get "Client ID" and "Client Secret" and  Add "Client Secret","Client ID" And select  auth_mode = 'oauth2'  in qb_master table.

## Step 07 ##

=> Open this Url : https://developer.intuit.com/app/developer/playground And In Select your App.

## Step 08 ##

=> If you select sendbox in select app you have to select devlopment in baseurl OR  you select production in select app you have to select Production in baseurl in “qb_master” table.

## Step 09 ##

=> Click on  Get Authorizationcode button for your reqiure service.

## Step 10 ##

=>  Get  Realm Id and Authorization Code and Add  in “qb_master” table.

## Step 11 ##

=>  After click on “Get tokens” button you will receive responce.

## Step 12 ##

=>  Get accessTokenKey and refreshTokenKey and add in “qb_master” table.

## Step 13 ##

=>  Open this Menu in Qb All Setting - Setting->Qb Setting  

## Step 14 ##

=> Click On Action button and select Qb Account Setting click on  fetch Qb Account button.

## Step 15 ##

=> Select All Item wise Credit Account Code save.

## Step 16 ##

=> Click On Action button and select Qb Default Setting click on Fetch QB Default data button.

## Step 17 ##

=> As your requirment add pament method,term,tax code after click on Push Qb Default data.